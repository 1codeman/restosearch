<?php
ini_set( "display_errors", true );  // change to false in deployment
date_default_timezone_set( "Asia/Manila" );  // http://www.php.net/manual/en/timezones.php
define( "DB_DSN", "mysql:host=localhost;dbname=restosearch" ); 
define( "DB_USERNAME", "root" ); //Set these values to your MySQL username
define( "DB_PASSWORD", "" ); //Set these values to your MySQL password

/**
*	---set path names : CLASS_PATH, which is the path to the class files, 
*	and TEMPLATE_PATH, which is where our script should look for the HTML template files.
*/
define( "CLASS_PATH", "classes" );
define( "TEMPLATE_PATH", "templates" );


define( "HOMEPAGE_NUM_ARTICLES",3 ); // controls the maximum number of article headlines to display on the site homepage.
define( "HOMEPAGE_NUM_RESTOS",2 ); // controls the maximum number of restos to display on the site homepage.
define( "ARTICLES_PAGE_PER_TABLE",3 ); // number of articles to be shown in a table for pagination
define( "CATEGORIES_PAGE_PER_TABLE",2 );// number of categories to be shown in a table for pagination
define( "MENU_PAGE_PER_TABLE",2 );// number of categories to be shown in a table for pagination

/**
*	---constants contain the login details for the CMS admin user.
*/	
define( "ADMIN_USERNAME", "admin" );
define( "ADMIN_PASSWORD", "d033e22ae348aeb5660fc2140aec35850c4da997" ); // used sha-1 hash (admin)

define( "USER_USERNAME", "user" );
define( "USER_PASSWORD", "user" ); 

define( "OWNER_IMAGE_PATH", "images/owners" );
define( "RESTO_IMAGE_PATH", "images/restos" );
define( "MENUITEM_IMAGE_PATH", "images/menuitems" );
define( "COVERPHOTO_IMAGE_PATH", "images/coverphotos" );
define( "USER_IMAGE_PATH", "images/users" );
define( "IMG_TYPE_FULLSIZE", "fullsize" );
define( "IMG_TYPE_THUMB", "thumb" );
define( "OWNER_THUMB_WIDTH", 120 );
define( "RESTO_THUMB_WIDTH", 120 );
define( "JPEG_QUALITY", 85 );

require( CLASS_PATH . "/article.php" ); //Since the Article class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/category.php" ); //Since the Category class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/menu.php" ); //Since the Menu class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/menuitem_category.php" ); //Since the ItemCategory class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/owner.php" ); //Since the Owner class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/resto.php" ); //Since the Resto class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/user.php" ); //Since the User class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/menu_item.php" ); //Since the Menu Item class file needed by all scripts in the app, we include it here.
require( CLASS_PATH . "/resto_table.php" );
require( CLASS_PATH . "/resto_feedback.php" );




/**
*	---simple function to handle any PHP exceptions that might be raised as our code runs.
*/

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}
set_exception_handler( 'handleException' ); //we set it as the exception handler by calling PHP's set_exception_handler() function.
?>