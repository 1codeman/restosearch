<?php
 
require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['owner-username'] ) ? $_SESSION['owner-username'] : "";

if (isset( $_SESSION['owner-username'] ) && $action == 'login') {
   restoDashboard();
  exit;
}


if ($action != "resto-login" && $action != "resto-logout" && $action != "resto-signup" && !$username ) {
    login();
  exit;
}

  
switch ( $action ) {
 
  case 'resto-signup':
    signup();
    break; 
  case 'resto-login':
    login();
    break; 
   case 'resto-logout':
    logout();
    break; 
  case 'listMenuItemCategories':
    listMenuItemCategories();
    break; 
  case 'newMenuItemCategory':
    newMenuItemCategory();
    break;
  case 'editMenuItemCategory':
    editMenuItemCategory();
    break;
  case 'editOwnerProfile':
    editOwnerProfile();
    break;
   case 'editRestoProfile':
    editRestoProfile();
    break;
  case 'editMenuItem':
    editMenuItem();
    break;
   case 'editRestoTable':
    editRestoTable();
    break;
  case 'deleteMenuItemCategory':
    deleteMenuItemCategory();
    break;
  case 'deleteMenuItem':
    deleteMenuItem();
    break;
  case 'deleteRestoTable':
    deleteRestoTable  ();
    break;
  case 'newMenuItem':
    newMenuItem();
    break;
  case 'newRestoTable':
    newRestoTable();
    break;
  case 'listMenuItem':
    listMenuItem();
    break;
  case 'listRestoTable':
    listRestoTable();
    break;
  case 'viewArticle':
    viewArticle();
    break;
   case 'newArticle':
    newArticle();
    break;
  case 'editArticle':
    editArticle();
    break;
  case 'deleteArticle':
    deleteArticle();
    break;
  case 'listCategories':
    listCategories();
    break;
  case 'newCategory':
    newCategory();
    break;
  case 'editCategory':
    editCategory();
    break;
  case 'deleteCategory':
    deleteCategory();
    break;
  case 'listArticles':
    listArticles();
    break;
  default:
   listMenuItem();


}
 


function restoDashboard() {


 $results = array();
 $results['menu'] = Menu::getById($_SESSION['restoId']);

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "menuNotFound" ) $results['errorMessage'] = "Error: Menu not found.";
  }
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "menuDeleted" ) $results['statusMessage'] = "Menu deleted.";
  }
 
 $results['pageTitle'] = "Resto Dashboard";
  require( TEMPLATE_PATH . "/resto/restoDashboard.php" );
}
 

 function signup() {
  $uname = isset( $_POST['uname'] ) ? $_POST['uname']: "";
  $email = isset( $_POST['email'] ) ? $_POST['email']: "";
  $results = array();
  $results['pageTitle'] = "Resto Signup";
   
//   // your secret key
//   $secret = "6LfEFQwTAAAAAGNUT7yin4y2udoEQQniyWIZH9K8";
//   // empty response
//   $response = null;
//   // check secret key
//   $reCaptcha = new ReCaptcha($secret);

//     // if submitted check response
//   if ($_POST["g-recaptcha-response"]) {
//       $response = $reCaptcha->verifyResponse(
//           $_SERVER["REMOTE_ADDR"],
//           $_POST["g-recaptcha-response"]
//       );
//   }else{
//      $results['errorMessage'] = "Robot ka ba?!";
//   }

// if ($response != null && $response->success) { 
  try
      {

         $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
         $sql = "SELECT * FROM owners WHERE uname = :uname or email = :email";
         $st = $conn->prepare( $sql );
         $st->bindValue( ":uname", $uname, PDO::PARAM_STR);
         $st->bindValue( ":email", $email, PDO::PARAM_STR);
         $st->execute();
         $row=$st->fetch(PDO::FETCH_ASSOC);
    
         if($row['uname']==$uname) {
            $results['errorMessage'] = "sorry username already taken !";
         }
         else if($row['email']==$email) {
             $results['errorMessage']  = "sorry email id already taken !";
         }
         else
        {
           $resto = new Resto;
           $resto->storeFormValues( $_POST);
           $restoId = $resto->insert();

           $owner = new Owner;
           $owner->storeFormValues( $_POST );
           $owner->insert($restoId);
           if ( isset( $_FILES['image'] ) ) $owner->storeUploadedImage( $_FILES['image'] );

           $menu = new Menu;
           $menu->storeFormValues( $_POST );
           $menu->insert($restoId);

        
           $results['statusMessage'] = "You have already Sign Up your resto.";

              
        }

      
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
 // }
   require( TEMPLATE_PATH . "/resto/loginForm.php" );
    
}
 
 
function login() {
 
  $results = array();
  $results['pageTitle'] = "Resto Login";
  $username = isset( $_POST['username'] ) ? $_POST['username']: "";
  $password = isset( $_POST['password'] ) ? $_POST['password']: "";


  
      if ( isset( $_POST['resto-login'] ) ) {
          
           $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
           $sql = "SELECT * FROM owners WHERE uname = :uname or email = :email LIMIT 1";
           $st = $conn->prepare( $sql );
           $st->bindValue( ":uname", $username, PDO::PARAM_STR);
           $st->bindValue( ":email", $username, PDO::PARAM_STR);
           $st->execute();
           $owner=$st->fetch(PDO::FETCH_ASSOC);
        // User has posted the login form: attempt to log the user in
     
        if ($st->rowCount()>0) {
            
            if($owner['password']==sha1($password))
            {
                  // Login successful: Create a session and redirect to the resto homepage
                  $_SESSION['owner-id'] = $owner['id'];
                  $_SESSION['owner-username'] = $owner['uname'];
                  $_SESSION['owner-email'] = $owner['email'];
                  $_SESSION['restoId'] = $owner['restoId'];
                  header( "Location: resto.php" );
            }
            else
            {
                  // Login failed: display an error message to the owner
              $results['errorMessage'] = "Incorrect username or password. Please try again.";
              require( TEMPLATE_PATH . "/resto/loginForm.php" );
            }
         
     
        } else {
     
          // Login failed: display an error message to the owner
          $results['errorMessage'] = "Incorrect username or password. Please try again.";
          require( TEMPLATE_PATH . "/resto/loginForm.php" );
        }
     
      } else {
     
        // User has not posted the login form yet: display the form
        require( TEMPLATE_PATH . "/resto/loginForm.php" );
      }


 
}
 
 
function logout() {
  unset( $_SESSION['owner-username'] );
  header( "Location: resto.php" );
}
 
 
function listMenuItemCategories() {

  $record_per_page = 3;  
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
  $start_from = ($page-1) * $record_per_page;  

  $results = array();
  $data = ItemCategory::getList($_SESSION['restoId'],$start_from,$record_per_page);
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  $results['categories'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Menu Item Categories";
 
  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "itemcategoryNotFound" ) $results['errorMessage'] = "Error: Item Category not found.";
    if ( $_GET['error'] == "itemcategoryContainsItems" ) $results['errorMessage'] = "Error: Item Category contains articles. Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "itemcategoryDeleted" ) $results['statusMessage'] = "Category deleted.";
  }
 
  require( TEMPLATE_PATH . "/resto/listMenuItemCategories.php" );
}
 

function listRestoTable() {

  $record_per_page = 5;  
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
  $start_from = ($page-1) * $record_per_page;  

  $results = array();
  $data = RestoTable::getList($start_from,$record_per_page,$_SESSION['restoId']);
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  $results['restotables'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Resto Tables";
 
  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "restotableNotFound" ) $results['errorMessage'] = "Error: Resto Table not found.";
    if ( $_GET['error'] == "restotableContainsItems" ) $results['errorMessage'] = "Error: Resto table contains data from another table. Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "restotableDeleted" ) $results['statusMessage'] = "Table deleted.";
  }
 
  require( TEMPLATE_PATH . "/resto/listRestoTable.php" );
}

 function listMenuItem() {
  $results = array( );
  $results['menu'] = Menu::getById($_SESSION['restoId']);

  $record_per_page = 5;  
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
  $start_from = ($page-1) * $record_per_page;  

  
  $data = MenuItem::getList($start_from,$record_per_page,$results['menu']->id);
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  $results['menuitems'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Menu Items";
 
  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "menuitemNotFound" ) $results['errorMessage'] = "Error: Menu Item not found.";
    if ( $_GET['error'] == "menuitemContainsItems" ) $results['errorMessage'] = "Error: Menu Item  contains . Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "menuitemDeleted" ) $results['statusMessage'] = "Menu Item deleted.";
  }
 
  require( TEMPLATE_PATH . "/resto/restoDashboard.php" );
}
 
   
function newRestoTable() {
 
  $results = array();
  $results['pageTitle'] = "New Resto Table";
  $results['formAction'] = "newRestoTable";
  $results['resto'] = Resto::getById($_SESSION['restoId']);

  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the Resto Table edit form: save the new table
    $restotable = new RestoTable;
    $restotable->storeFormValues( $_POST );
    $restotable->insert();
    // if ( isset( $_FILES['image'] ) ) $menuitem->storeUploadedImage( $_FILES['image'] );
     header( "Location: resto.php?action=listRestoTable&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the main list
     header( "Location: resto.php?action=listRestoTable" );
  } else {
 
    // User has not posted the resto table edit form yet: display the form
    $results['restotable'] = new RestoTable;
    require( TEMPLATE_PATH . "/resto/editRestoTable.php" );
  }
 
}
 

function newMenuItem() {
 
  $results = array();
  $results['pageTitle'] = "New Menu Item";
  $results['formAction'] = "newMenuItem";
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  $results['menu'] = Menu::getById($_SESSION['restoId']);

  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the menu item edit form: save the new article
    $menuitem = new MenuItem;
    $menuitem->storeFormValues( $_POST );
    $menuitem->insert();
    if ( isset( $_FILES['image'] ) ) $menuitem->storeUploadedImage( $_FILES['image'] );
    header( "Location: resto.php?status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the article list
    header( "Location: resto.php" );
  } else {
 
    // User has not posted the menu item edit form yet: display the form
    $results['menuitem'] = new MenuItem;
    $data = ItemCategory::getList($_SESSION['restoId']);
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/resto/editMenuItem.php" );
  }
 
}
 
 
function newMenuItemCategory() {
 
  $results = array();
  $results['pageTitle'] = "New Menu Item Category";
  $results['formAction'] = "newMenuItemCategory";
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the category edit form: save the new category
    $category = new ItemCategory;
    $category->storeFormValues( $_POST );
    $category->insert($_SESSION['restoId']);
    header( "Location: resto.php?action=listMenuItemCategories&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the category list
    header( "Location: resto.php?action=listMenuItemCategories" );
  } else {
 
    // User has not posted the category edit form yet: display the form
    $results['category'] = new ItemCategory;
    require( TEMPLATE_PATH . "/resto/editMenuItemCategory.php" );
  }
 
}

function editOwnerProfile() {
 
  $results = array();
  $results['pageTitle'] = "Owner Profile";
  $results['formAction'] = "editOwnerProfile";
  $results['resto'] = Resto::getById($_SESSION['restoId']);

    if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "ownerNotFound" ) $results['errorMessage'] = "Error: Owner not found.";
    if ( $_GET['error'] == "ownerContainsItems" ) $results['errorMessage'] = "Error: Item Category contains articles. Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your Profile has been updated.";
  }

  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the owner edit form: save the owner changes
 
    if ( !$owner = Owner::getByUsername( $_POST['uname'] ) ) {
      header( "Location: resto.php?action=editOwnerProfile&error=OwnerNotFound" );
      return;
    }
 
    $owner->storeFormValues( $_POST );  
    if ( isset($_POST['deleteImage']) && $_POST['deleteImage'] == "yes" ) $owner->deleteImages();
    $owner->update();
    if ( isset( $_FILES['image'] ) ) $owner->storeUploadedImage( $_FILES['image'] );
    header( "Location: resto.php?action=editOwnerProfile&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // Owner has cancelled their edits: return to the category list
    header( "Location: resto.php?action=editOwnerProfile" );
  } else {
 
    // User has not posted the owner edit form yet: display the form
    $results['owner'] = Owner::getByUsername( $_SESSION['owner-username']);
    require( TEMPLATE_PATH . "/resto/editOwnerProfile.php" );
  }
 
}

 function editRestoProfile() {
 
  $results = array();
  $results['pageTitle'] = "Resto Profile";
  $results['formAction'] = "editRestoProfile";
  $results['resto'] = Resto::getById($_SESSION['restoId']);

    if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "restoNotFound" ) $results['errorMessage'] = "Error: Resto not found.";
    if ( $_GET['error'] == "restoContainsItems" ) $results['errorMessage'] = "Error: Item Category contains articles. Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your Resto Profile has been updated.";
  }

  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the owner edit form: save the owner changes
 
    if ( !$resto = Resto::getById((int)$_POST['id'] ) ) {
      header( "Location: resto.php?action=editRestoProfile&error=RestoNotFound" );
      return;

    }

    $resto->storeFormValues($_POST);  
    if ( isset($_POST['deleteImage']) && $_POST['deleteImage'] == "yes" ) $resto->deleteImages();
    if ( isset($_POST['deleteImage2']) && $_POST['deleteImage2'] == "yes" ) $resto->deleteImages2();
    $resto->update();
    if ( isset( $_FILES['image'] ) ) $resto->storeUploadedImage( $_FILES['image'] );  
    if ( isset( $_FILES['image2'] ) ) $resto->storeUploadedImage2( $_FILES['image2'] );  
  

    header( "Location: resto.php?action=editRestoProfile&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // Resto has cancelled their edits: return to the restoprofile
    header( "Location: resto.php?action=editRestoProfile" );
  } else {
 
    // User has not posted the owner edit form yet: display the form
    $results['resto'] = Resto::getById( (int)$_SESSION['restoId'] );
    require( TEMPLATE_PATH . "/resto/editRestoProfile.php" );
  }
 
}


function editRestoTable() {
 
  $results = array();
  $results['pageTitle'] = "Edit Resto Table";
  $results['formAction'] = "editRestoTable";
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the category edit form: save the category changes
 
    if ( !$restotable = RestoTable::getById( (int)$_POST['tableId'] ) ) {
      header( "Location: resto.php?action=listRestoTable&error=restotableNotFound" );
      return;
    }
 
    $restotable->storeFormValues( $_POST );
    $restotable->update();
    header( "Location: resto.php?action=listRestoTable&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the Resto Table List
    header( "Location: resto.php?action=listRestoTable" );
  } else {
 
    // User has not posted the category edit form yet: display the form
    $results['restotable'] = RestoTable::getById( (int)$_GET['tableId'] );
    require( TEMPLATE_PATH . "/resto/editRestoTable.php" );
  }
 
}

function editMenuItemCategory() {
 
  $results = array();
  $results['pageTitle'] = "Edit Menu Item Category";
  $results['formAction'] = "editMenuItemCategory";
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the category edit form: save the category changes
 
    if ( !$category = ItemCategory::getById( (int)$_POST['categoryId'] ) ) {
      header( "Location: resto.php?action=listMenuItemCategories&error=itemcategoryNotFound" );
      return;
    }
 
    $category->storeFormValues( $_POST );
    $category->update();
    header( "Location: resto.php?action=listMenuItemCategories&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the category list
    header( "Location: resto.php?action=listMenuItemCategories" );
  } else {
 
    // User has not posted the category edit form yet: display the form
    $results['category'] = ItemCategory::getById( (int)$_GET['categoryId'] );
    require( TEMPLATE_PATH . "/resto/editMenuItemCategory.php" );
  }
 
}
 

 function editMenuItem() {
 
  $results = array();
  $results['pageTitle'] = "Edit Menu Item";
  $results['formAction'] = "editMenuItem";
   $results['menu'] = Menu::getById($_SESSION['restoId']);
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the menuitem edit form: save the category changes
 
    if ( !$menuitem = MenuItem::getById( (int)$_POST['itemId'] ) ) {
      header( "Location: resto.php?action=listMenuItem&error=menuitemNotFound" );
      return;
    }


    $menuitem->storeFormValues( $_POST );
    if ( isset($_POST['deleteImage']) && $_POST['deleteImage'] == "yes" ) $menuitem->deleteImages();
    $menuitem->update();
    if ( isset( $_FILES['image'] ) ) $menuitem->storeUploadedImage( $_FILES['image'] );

 
    header( "Location: resto.php?action=listMenuItem&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the category list
    header( "Location: resto.php?action=listMenuItem" );
  } else {
 // User has not posted the article edit form yet: display the form
    if($results['menuitem'] = MenuItem::getById( (int)$_GET['itemId'] )){
  // User has not posted the menu item edit form yet: display the form
      $results['menuitem'] = MenuItem::getById( (int)$_GET['itemId'] );
      $data = ItemCategory::getList($_SESSION['restoId']);
      $results['categories'] = $data['results'];
    }else{
       header( "Location: resto.php?action=listMenuItem&status=idNotFOund" );
    }
    
    require( TEMPLATE_PATH . "/resto/editMenuItem.php" );
  }
 
}
 
 
 
function deleteMenuItemCategory() {
 
  if ( !$category = ItemCategory::getById( (int)$_GET['categoryId'] ) ) {
    header( "Location: resto.php?action=listMenuItemCategories&error=itemcategoryNotFound" );
    return;
  }
 
  // $articles = Article::getList( 1000000, $category->id );
 
  // if ( $articles['totalRows'] > 0 ) {
  //   header( "Location: resto.php?action=listCategories&error=categoryContainsArticles" );
  //   return;
  // }
 
  $category->delete();
  header( "Location: resto.php?action=listMenuItemCategories&status=itemcategoryDeleted" );
}
 

 
 
function deleteMenuItem() {
 
  if ( !$menuitem = MenuItem::getById( (int)$_GET['itemId']) ) {
    header( "Location: resto.php?action=listMenuItem&error=menuItemNotFound" );
    return;
  }
  $menuitem->deleteImages();
  $menuitem->delete();
  header( "Location: resto.php?action=listMenuItem&status=itemcategoryDeleted" );
}
 
 
  
function deleteRestoTable() {
 
  if ( !$restotable = RestoTable::getById( (int)$_GET['tableId']) ) {
    header( "Location: resto.php?action=listRestoTable&error=restotableNotFound" );
    return;
  }
  $restotable->delete();
  header( "Location: resto.php?action=listRestoTable&status=restotableDeleted" );
}
 

 function newArticle() {
 
  $results = array();
  $results['pageTitle'] = "New Article";
  $results['formAction'] = "newArticle";
 $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the article edit form: save the new article
    $article = new Article;
    $article->storeFormValues( $_POST );
    $article->insert();
    header( "Location: resto.php?action=listArticles&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the article list
    header( "Location: resto.php?action=listArticles" );
  } else {
 
    // User has not posted the article edit form yet: display the form
    $results['article'] = new Article;
    $data = Category::getList();
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/resto/editArticle.php" );
  }
 
}
 
 
function editArticle() {
 
  $results = array();
  $results['pageTitle'] = "Edit Article";
  $results['formAction'] = "editArticle";
 $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the article edit form: save the article changes
 
    if ( !$article = Article::getById( (int)$_POST['articleId'] ) ) {
      header( "Location: resto.php?error=articleNotFound" );
      return;
    }
 
    $article->storeFormValues( $_POST );
    $article->update();
    header( "Location: resto.php?action=listArticles&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the article list
     header( "Location: resto.php?action=listArticles" );
  } else {
 
    // User has not posted the article edit form yet: display the form
    $results['article'] = Article::getById( (int)$_GET['articleId'] );
    $data = Category::getList();
    $results['categories'] = $data['results'];
    require( TEMPLATE_PATH . "/resto/editArticle.php" );
  }
 
}
 
 
function deleteArticle() {
 
  if ( !$article = Article::getById( (int)$_GET['articleId'] ) ) {
    header( "Location: resto.php?error=articleNotFound" );
    return;
  }
 
  $article->delete();

  header( "Location:resto.php?action=listArticles&status=articleDeleted" );
}
 
 
function listArticles() {
  
  $record_per_page = ARTICLES_PAGE_PER_TABLE;  
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
  $start_from = ($page-1) * $record_per_page;  


  $results = array();
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  $data = Article::getList($start_from,$record_per_page,NULL,$_SESSION['owner-id']);
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  $results['pageTitle'] = "All Articles";
 
  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
  }
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
  }
 
  require( TEMPLATE_PATH . "/resto/listArticles.php" );
}
 
 
function listCategories() {

  $record_per_page = CATEGORIES_PAGE_PER_TABLE;  
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
  $start_from = ($page-1) * $record_per_page;  

  $results = array();
  $data = Category::getList($start_from,$record_per_page,$_SESSION['owner-id']);
  $results['resto'] = Resto::getById($_SESSION['restoId']);
  $results['categories'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "Article Categories";
 
  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "categoryNotFound" ) $results['errorMessage'] = "Error: Category not found.";
    if ( $_GET['error'] == "categoryContainsArticles" ) $results['errorMessage'] = "Error: Category contains articles. Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "categoryDeleted" ) $results['statusMessage'] = "Category deleted.";
  }
 
  require( TEMPLATE_PATH . "/resto/listCategories.php" );
}
 
 
function newCategory() {
 
  $results = array();
  $results['pageTitle'] = "New Article Category";
  $results['formAction'] = "newCategory";
 $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the category edit form: save the new category
    $category = new Category;
    $category->storeFormValues( $_POST );
    $category->insert();
    header( "Location: resto.php?action=listCategories&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the category list
    header( "Location: resto.php?action=listCategories" );
  } else {
 
    // User has not posted the category edit form yet: display the form
    $results['category'] = new Category;
    require( TEMPLATE_PATH . "/resto/editCategory.php" );
  }
 
}
 
 
function editCategory() {
 
  $results = array();
  $results['pageTitle'] = "Edit Article Category";
  $results['formAction'] = "editCategory";
 $results['resto'] = Resto::getById($_SESSION['restoId']);
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the category edit form: save the category changes
 
    if ( !$category = Category::getById( (int)$_POST['categoryId'] ) ) {
      header( "Location: resto.php?action=listCategories&error=categoryNotFound" );
      return;
    }
 
    $category->storeFormValues( $_POST );
    $category->update();
    header( "Location: resto.php?action=listCategories&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // User has cancelled their edits: return to the category list
    header( "Location: resto.php?action=listCategories" );
  } else {
 
    // User has not posted the category edit form yet: display the form
    $results['category'] = Category::getById( (int)$_GET['categoryId'] );
    require( TEMPLATE_PATH . "/resto/editCategory.php" );
  }
 
}
 
 
function deleteCategory() {
 
  if ( !$category = Category::getById( (int)$_GET['categoryId'] ) ) {
    header( "Location: resto.php?action=listCategories&error=categoryNotFound" );
    return;
  }
 
  $articles = Article::getList(0, 1000000, $category->id );
 
  if ( $articles['totalRows'] > 0 ) {
    header( "Location: resto.php?action=listCategories&error=categoryContainsArticles");
    return;
  }
 
  $category->delete();
  header( "Location: resto.php?action=listCategories&status=categoryDeleted" );
}
 
?>