
   </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>
    <!-- /.wrap -->
  <!-- Footer -->
        <div id="footer">
              <div class="container">
                <p class="text-muted credit">#Capstone101&copy; 2016. All rights reserved.  <a href="resto.php">Resto</a>&nbsp  | &nbsp <a href=".?action=user-login">User</a></p>
              </div>    


    <!-- jQuery -->
    <script src="https://soulsearching-lailamari.c9users.io/restosearch/js/jquery.js"></script>

    <!-- CKEDITOR-->
    <script src="https://soulsearching-lailamari.c9users.io/restosearch/ckeditor/ckeditor.js"></script>
    <!-- CKFINDER-->
    <script src="https://soulsearching-lailamari.c9users.io/restosearch/ckfinder/ckfinder.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://soulsearching-lailamari.c9users.io/restosearch/js/bootstrap.min.js"></script>
    
    <!-- mapbox -->
    <script src='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.js'></script>
     <script>
            CKEDITOR.replace( 'content', {
                filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
            } );

            
    </script>

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
<link href="https://soulsearching-lailamari.c9users.io/restosearch/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />

<!-- optionally if you need to use a theme, then include the theme file as mentioned below -->
<link href="https://soulsearching-lailamari.c9users.io/restosearch/css/theme-krajee-svg.css" media="all" rel="stylesheet" type="text/css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.js"></script>
<script src="https://soulsearching-lailamari.c9users.io/restosearch/js/star-rating.js" type="text/javascript"></script>

<!-- optionally if you need translation for your language then include locale file as mentioned below -->
<script src="https://soulsearching-lailamari.c9users.io/restosearch/js/star-rating_locale_LANG.js"></script>

<!-- ++++++++++++++++++++++++++ MAPBOX +++++++++++++++++++++++++++++++++ -->

 <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' />
    <script src='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.js'></script>
    
<style>


</style>

<script>
var coordinates = document.getElementById('coordinates');
var lon = "<?php echo $results['resto']->lon ?>"
var lat = "<?php echo $results['resto']->lat ?>"

L.mapbox.accessToken = 'pk.eyJ1IjoiZHRyaW5pZGFkaXYiLCJhIjoiY2lqYTA5aHN0MDA0MHVsa245NGZpM3NuNSJ9.9bO-Y4WM8tHMPZbRZVyfMQ';

  if(lon > 0 && lat > 0){
    var map = L.mapbox.map('map', 'mapbox.streets')
    .setView([lat, lon], 15);
  }else{
     var map = L.mapbox.map('map', 'mapbox.streets')
    .setView([13.620838, 123.20328], 15);
  }



  if(lon > 0 && lat > 0){
   var marker = L.marker([lat, lon], {
    icon: L.mapbox.marker.icon({
      'marker-color': '#f86767'
    }),
    draggable: true
}).addTo(map);

  }else{
     var marker = L.marker([13.620838, 123.20328], {
    icon: L.mapbox.marker.icon({
      'marker-color': '#f86767'
    }),
    draggable: true
}).addTo(map);

  }



// every time the marker is dragged, update the coordinates container
marker.on('dragend', ondragend);

// Set the initial marker coordinate on load.
ondragend();

function ondragend() {
    var m = marker.getLatLng();
    if(lon > 0  && lat >0){
       coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
     }else{

       coordinates.innerHTML =  "Your Establishment is not yet registered in the MAP!</br>" + 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
     }
   
    document.getElementById("coordinates1").value = m.lng;
    document.getElementById("coordinates2").value = m.lat;
   
}

</script>



  </body>
</html>

