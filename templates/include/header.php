<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <meta name="description" content="">
    <meta name="author" content="">

	<!--PHP function htmlspecialchars() encodes any special HTML characters, 
    	such as <, >, and &, into their HTML entity equivalents (&lt;, &gt;, and &amp;) -->
   	<title><?php echo htmlspecialchars( $results['pageTitle'] )?></title>

     <link href="https://soulsearching-lailamari.c9users.io/restosearch/css/style.css" rel="stylesheet">
     <link href="https://soulsearching-lailamari.c9users.io/restosearch/css/signin.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="https://soulsearching-lailamari.c9users.io/restosearch/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="https://soulsearching-lailamari.c9users.io/restosearch/css/blog-home.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


  <!-- mapbox CSS -->
    <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' />
    <script src='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.js'></script>
  </head>

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
<link href="https://soulsearching-lailamari.c9users.io/restosearch/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />

<!-- optionally if you need to use a theme, then include the theme file as mentioned below -->
<link href="https://soulsearching-lailamari.c9users.io/restosearch/css/theme-krajee-svg.css" media="all" rel="stylesheet" type="text/css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.js"></script>
<script src="https://soulsearching-lailamari.c9users.io/restosearch/js/star-rating.js" type="text/javascript"></script>

<!-- optionally if you need translation for your language then include locale file as mentioned below -->
<script src="https://soulsearching-lailamari.c9users.io/restosearch/js/star-rating_locale_LANG.js"></script>

  <body>
  <div id="wrap">
     <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top nav-down" role="navigation" style="background-color:#222222">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
              
                <ul class="nav navbar-nav navbar-right"  >
                 <li>
                   <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
                
                  
              </li>
              
            <?php if(isset($_SESSION['user-username'])){ ?>

                
                <?php if ( $results['user'] && $imagePath = $results['user']->getImagePath() ) { ?>

            <img style="padding-top:10px;padding-bottom:10px;padding-right:5px"  width="30px"  src="<?php echo $imagePath ?>" alt="User Image"/>
         
   

      <?php } else{?>       
                <img style="padding-top:10px;padding-bottom:10px;padding-right:5px"  width="30px"  src="http://placehold.it/150?text=NULL" alt="User Image" />
     <?php } ?>     

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><b><?php echo htmlspecialchars( $_SESSION['user-username']) ?></b><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                          <li><a href="?action=profile">Profile</a></li>
                          <li><a href="?action=user-logout">Sign Out</a></li>
                
              </li>
              
              
                </ul>

            <?php }?>
            
            
            
            
               <?php if(!isset($_SESSION['user-username'])){ ?>

            <ul class="nav navbar-nav navbar-right">
              <li><a href=".?action=user-login">Login</a></li>
              

            </ul>
            
            <?php }?>
            
            
        <!--     <li><p class="navbar-text">Already have an account?</p></li>
                </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Sign in<span class="caret"></span></a>
                                                <ul class="dropdown-menu"><hr>
                          <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav" style="padding-right:14px;padding-left:14px; min-width: 250px">
                    <div class="form-group">
                       <label class="sr-only" for="exampleInputEmail2">Email address</label>
                       <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                    </div>
                    <div class="form-group">
                       <label class="sr-only" for="exampleInputPassword2">Password</label>
                       <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                             <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                    </div>
        
                 </form>
                  </ul>
              </li>
                </ul>
 -->
             <!--   <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input style="height:30px" type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input style="height:30px" type="text" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button  type="submit" class="btn btn-default">Sign In</button>
                </form>-->

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
        
        

    </nav>
    
    
    
        <header id="new-section" style="background-color:#333333;height:195px;">
            
        <br><br><br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              
        <a href=".">
            <img src="https://soulsearching-lailamari.c9users.io/restosearch/images/ncfflogo.png" alt="NCFF" id="logo" width="250px">
            
        </a>

     
</header>
<br><br>
    <!-- Page Content -->
    <div class="container">

        <div class="row">


   