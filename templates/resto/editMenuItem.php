<?php include "templates/include/header.php" ?>
<?php include "templates/include/resto.php" ?>
      
       <script>
 
      // Prevents file upload hangs in Mac Safari
      // Inspired by http://airbladesoftware.com/notes/note-to-self-prevent-uploads-hanging-in-safari
 
      function closeKeepAlive() {
        if ( /AppleWebKit|MSIE/.test( navigator.userAgent) ) {
          var xhr = new XMLHttpRequest();
          xhr.open( "GET", "/ping/close", false );
          xhr.send();
        }
      }
 
      </script>

      <center><h1><?php echo $results['pageTitle']?></h1></center>
 
        <form action="resto.php?action=<?php echo $results['formAction']?>" method="post" enctype="multipart/form-data" onsubmit="closeKeepAlive()">
        <input type="hidden" name="itemId" value="<?php echo $results['menuitem']->id ?>"/>
         <input type="hidden" name="menuID" id="menuID" value="<?php echo $results['menu']->id ?>" />
 
<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>




         <?php if ( $results['menuitem'] && $imagePath = $results['menuitem']->getImagePath() ) { ?>
              
           <center> <img class="img-circle" id="itemImage" src="<?php echo $imagePath ?>" alt="Menu Item Image" width ="150" height = "150"/>
         
        <div class="form-group ">
            <input type="checkbox" name="deleteImage" id="deleteImage" value="yes"/ > <label for="deleteImage">Delete</label>
    
          </div>
    </div>

      <?php } else{?>      
                    <div class="form-group ">
                <center><img class="img-circle" id="itemImage" src="http://placehold.it/150?text=NULL" alt="MenuItem Image" width ="150" height = "150"/>
                </div>
     <?php } ?>     
      <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
            <input type="file" name="image" id="image" placeholder="Choose an image to upload" maxlength="255" />
         
          </div>

      
             </div>
            </div>


          <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3  selectContainer">
                <label class="control-label">Menu Item Name</label>
                <input style="width = 70%" class="form-control" type="text" name="name" id="name" placeholder="Name of the Item" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['menuitem']->name )?>" />
              </div>
             </div>
            </div>

            <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3  selectContainer">
                <label class="control-label">Description</label>
                <input class="form-control" type="text" name="description" id="description" placeholder="Descripton of the Item" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['menuitem']->description )?>" />
              </div>
             </div>
            </div>
 

            <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3   selectContainer">
                <label class="control-label">Price</label>
                <input class="form-control" type="number"  step="any" name="price" id="price" placeholder="Price of the Item" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['menuitem']->price )?>" />
              </div>
             </div>
            </div>


              <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3  selectContainer">
                <label class="control-label">Menu Item Category</label>
                <select class="form-control" name="categoryID">
              <option value="0"<?php echo !$results['menuitem']->categoryID ? " selected" : ""?>>(none)</option>
            <?php foreach ( $results['categories'] as $category ) { ?>
              <option value="<?php echo $category->id?>"<?php echo ( $category->id == $results['menuitem']->categoryID ) ? " selected" : ""?>><?php echo htmlspecialchars( $category->name )?></option>
            <?php } ?>
            </select>

              </div>
             </div>
            </div>


         <div class="col-lg-6 col-lg-offset-3  selectContainer" align = "right" class="buttons" >
          <input class="btn btn-primary" type="submit" name="saveChanges" value="Save Changes" />
          <input class="btn btn-danger" type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>
 
      </form>
 
 
 
<?php include "templates/include/footer.php" ?>