<?php include "templates/include/header.php" ?>
<?php include "templates/include/resto.php" ?>
 
      <h1><?php echo $results['pageTitle']?></h1>
 
      <form action="resto.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="tableId" value="<?php echo $results['restotable']->id ?>"/>
        <input type="hidden" name="restoID" value="<?php echo $results['resto']->id ?>"/>
<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

          <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">Table Name</label>
                <input style="width = 70%" class="form-control" type="text" name="table_name" id="table_name" placeholder="Name of the table" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['restotable']->table_name )?>" />
              </div>
             </div>
            </div>

            <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">Table Description</label>
                <input class="form-control" type="text" name="table_description" id="description" placeholder="Descripton of the category" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['restotable']->table_description )?>" />
              </div>
             </div>
            </div>

            <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">Table Capacity</label>
                <input class="form-control" type="number" name="capacity" id="capacity" placeholder="Capacity of the table" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['restotable']->capacity )?>" />
              </div>
             </div>
            </div>
 
         <div class="col-lg-6 col-lg-offset-3 selectContainer" align = "right" class="buttons" >
          <input class="btn btn-primary" type="submit" name="saveChanges" value="Save Changes" />
          <input class="btn btn-danger" type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>
 
      </form>
 
 
 
<?php include "templates/include/footer.php" ?>