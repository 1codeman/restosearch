<?php include "templates/include/header.php" ?>
 <div class="wrapper" >
<center><br><br>
<div class="tabbable" >
  <ul class="nav nav-tabs" >
    <li class="active"><a href="#pane1" data-toggle="tab">Sign Up</a></li>
    <li><a href="#pane2" data-toggle="tab">Resto Login</a></li>
  </ul>

  <div class="tab-content">
    <div id="pane1" class="tab-pane active">
     
                            <form class = "form-signin" action="?action=resto-signup" method="post" >
                            <center><h4 class="form-signin-heading page-header">Resto Sign Up</h4></center>
                            <hr>
                              <input type="hidden" name="resto-signup" value="true" />

                      <?php if ( isset( $results['errorMessage'] ) ) { ?>

                                <div class="alert alert-danger">
                                            <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                                       </div>

                      <?php } ?>

                        <?php if ( isset( $results['statusMessage'] ) ) { ?>

                                <div class="alert alert-info">
                                            <i class="glyphicon glyphicon-ok"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                                       </div>

                      <?php } ?>

                    
                            <div style="padding-bottom:5px">

                                   <input class="form-control" type="email" name="email" id="email" placeholder="email" required autofocus maxlength="20" />
                             </div> 
                             <div style="padding-bottom:5px">
                                  <input class="form-control" type="text" name="uname" id="uname" placeholder="username" required autofocus maxlength="20" />
                               </div> 

                              <div style="padding-bottom:5px">
                                  <input class="form-control"  type="password" name="password" id="password" placeholder="password" required maxlength="20" />
                            </div> 

                            <div style="padding-bottom:5px">

                                   <input class="form-control" type="text" name="resto_name" id="resto_name" placeholder="resto name" required autofocus maxlength="20" />
                             </div> 
                             <div style="padding-bottom:5px">
                                  <input class="form-control" type="text" name="resto_description" id="resto_description" placeholder="description" required autofocus maxlength="100" />
                               </div> 

                              <div style="padding-bottom:5px">
                                  <input class="form-control" type="text" name="menu_name" id="menu_name" placeholder="menu name" required autofocus maxlength="100" />
                               </div> 

                                <div style="padding-bottom:5px">
                                  <input class="form-control" type="text" name="menu_description" id="menu_description" placeholder="menu description" required autofocus maxlength="100" />
                               </div> 



                         <!--    <div class="g-recaptcha" data-sitekey="6LfEFQwTAAAAANT50P6QI37lzc9eW1AEh3UXJ1FN"></div> -->

                           <hr>
                          
                              <div class="buttons">
                                <input class="btn btn-lg btn-danger btn-block" type="submit" name="resto-signup" value="Sign Up" />
                              </div> 

                            </form>
    </div>

    <div id="pane2" class="tab-pane">
   
      <form class = "form-signin" action="?action=resto-login" method="post" >
      <center><h3 class="form-signin-heading page-header">RestoLogin</h3></center>
      <hr>
        <input type="hidden" name="resto-login" value="true" />

<?php if ( isset( $results['errorMessage'] ) ) { ?>

          <div class="alert alert-danger">
                      <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                 </div>

<?php } ?>

       <div  style="padding-bottom:5px">
            <input  style="padding-bottom:5px"class="form-control" type="text" name="username" id="username" placeholder="username" required autofocus maxlength="20" />
        </div>
        
            <input class="form-control"  type="password" name="password" id="password" placeholder="password" required maxlength="20" />
     <hr>


        <div class="buttons">
          <input class="btn btn-lg btn-primary btn-block" type="submit" name="resto-login" value="Login" />
        </div> 

      </form>
    </div>
  </div><!-- /.tab-content -->
</div><!-- /.tabbable -->

</div>
<?php include "templates/include/footer.php" ?>