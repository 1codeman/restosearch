<?php include "templates/include/header.php" ?>
<?php include "templates/include/resto.php" ?>
  <script>
 
      // Prevents file upload hangs in Mac Safari
      // Inspired by http://airbladesoftware.com/notes/note-to-self-prevent-uploads-hanging-in-safari
 
      function closeKeepAlive() {
        if ( /AppleWebKit|MSIE/.test( navigator.userAgent) ) {
          var xhr = new XMLHttpRequest();
          xhr.open( "GET", "/ping/close", false );
          xhr.send();
        }
      }
 
      </script>
 
      <center><h1><?php echo $results['pageTitle']?></h1></center>

           
       
  

      
      
      <form action="resto.php?action=<?php echo $results['formAction']?>" method="post" align="center" enctype="multipart/form-data" onsubmit="closeKeepAlive()">
        <input type="hidden" name="id" value="<?php echo $results['resto']->id ?>"/>
 
<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>


<?php if ( $results['resto'] && $imagePath = $results['resto']->getImagePath() ) { ?>
      <div class="form-group ">
            <img class="img-circle" id="restoImage" src="<?php echo $imagePath ?>" alt="Resto Image" width ="150" height = "150"/>
         
        <div class="form-group ">
            <input type="checkbox" name="deleteImage" id="deleteImage" value="yes"/ > <label for="deleteImage">Delete</label>
    
          </div>
    </div>

      <?php } else{?>       
                <img class="img-circle" id="restoImage" src="http://placehold.it/150?text=NULL" alt="Resto Image" width ="150" height = "150"/>
     <?php } ?>     
      <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
            <input type="file" name="image" id="image" placeholder="Choose an image to upload" maxlength="255" />
         
          </div>

      
             </div>
            </div>


<?php if ( $results['resto'] && $imagePath = $results['resto']->getImagePath2() ) { ?>
      <div class="form-group ">
            <img class="" id="restoImage2" src="<?php echo $imagePath ?>" alt="Resto Image" width ="750" height = "350"/>
         
        <div class="form-group ">
            <input type="checkbox" name="deleteImage2" id="deleteImage2" value="yes"/ > <label for="deleteImage2">Delete</label>
    
          </div>
    </div>

      <?php } else{?>       
                <img class="" id="restoImage2" src="http://placehold.it/750X350?text=COVER PHOTO" alt="Resto Image" width ="750" height = "350"/>
     <?php } ?>     
      <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
            <input type="file" name="image2" id="image2" placeholder="Choose an image to upload" maxlength="255" />
         
          </div>

      
             </div>
            </div>


          <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
                <label class="control-label">Name</label>
                <input style="width = 70%" class="form-control" type="text" name="resto_name" id="resto_name" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['resto']->resto_name )?>" />
              </div>

      
             </div>
            </div>

           <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
                <label class="control-label">Description</label>
                <input style="width = 70%" class="form-control" type="text" name="resto_description" id="resto_description" placeholder=""  autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['resto']->resto_description )?>" />
              </div>

      
             </div>
            </div>

          <div class="form-group ">
           <div class="row " >
              <div class="col-lg-6 col-lg-offset-3 selectContainer ">
              <label class="control-label">Contact No</label>
              <input style="width = 70%" class="form-control" type="text" name="contactNo" id="contactNo" placeholder=""  autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['resto']->contactNo )?>" />
            </div>

              
           </div>
          </div>


         <div class="form-group ">
           <div class="row " >
              <div class="col-lg-6 col-lg-offset-3 selectContainer ">
              <label class="control-label">Category</label>
              <input style="width = 70%" class="form-control" type="text" name="category" id="category" placeholder=""  autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['resto']->category )?>" />
            </div>

              
           </div>
          </div>


        <div class="form-group ">
           <div class="row " >
              <div class="col-lg-6 col-lg-offset-3 selectContainer ">
              <label class="control-label">Address</label>
              <input style="width = 70%" class="form-control" type="text" name="address" id="address" placeholder=""  autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['resto']->address )?>" />
            </div>

              
           </div>
          </div>

            
               <input style="width = 70%" class="form-control" type="hidden" name="lon" id="coordinates1" placeholder=""  autofocus maxlength="255" />
       
               <input style="width = 70%" class="form-control" type="hidden" name="lat" id="coordinates2" placeholder=""  autofocus maxlength="255" />
       




        <div class="form-group ">
          <div class="container">
           <div class="row " >
            <div class="col-lg-6 col-lg-offset-3 selectContainer ">
          <div id='map'  style="width:100%; height:300px;"></div>
              </div>
                   
         <div class="col-lg-6 col-lg-offset-3 selectContainer ">
            
            <pre id='coordinates' class='ui-coordinates'></pre>
              </div>

           </div>



          </div>
        </div>
     

         <div class="col-lg-6 col-lg-offset-3 selectContainer" align = "right" class="buttons" >
          <input class="btn btn-primary" type="submit" name="saveChanges" value="Save Changes" />
           <a class="btn btn-danger" href="resto.php?action=restoDashboard">Back to Main Dashboard</a><br><br><hr>
        </div>
 
      </form>
   





<?php include "templates/include/footer.php" ?>