<?php include "templates/include/header.php" ?>
<?php include "templates/include/resto.php" ?>
 <script>
 
      // Prevents file upload hangs in Mac Safari
      // Inspired by http://airbladesoftware.com/notes/note-to-self-prevent-uploads-hanging-in-safari
 
      function closeKeepAlive() {
        if ( /AppleWebKit|MSIE/.test( navigator.userAgent) ) {
          var xhr = new XMLHttpRequest();
          xhr.open( "GET", "/ping/close", false );
          xhr.send();
        }
      }
 
      </script>
 
      <center><h1><?php echo $results['pageTitle']?></h1></center>

           
       
  

      <form action="resto.php?action=<?php echo $results['formAction']?>" method="post" align="center" enctype="multipart/form-data" onsubmit="closeKeepAlive()">
        <input type="hidden" name="uname" value="<?php echo $results['owner']->uname ?>"/>
 
<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>


<?php if ( $results['owner'] && $imagePath = $results['owner']->getImagePath() ) { ?>
      <div class="form-group ">
            <img class="img-circle" id="ownerImage" src="<?php echo $imagePath ?>" alt="Owner Image" width ="150" height = "150"/>
         
        <div class="form-group ">
            <input type="checkbox" name="deleteImage" id="deleteImage" value="yes"/ > <label for="deleteImage">Delete</label>
    
          </div>
    </div>

      <?php } else{?>       
                <img class="img-circle" id="ownerImage" src="http://placehold.it/150?text=NULL" alt="Owner Image" width ="150" height = "150"/>
     <?php } ?>     
      <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
            <input type="file" name="image" id="image" placeholder="Choose an image to upload" maxlength="255" />
         
          </div>

      
             </div>
            </div>



          <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
                <label class="control-label">Email</label>
                <input style="width = 70%" class="form-control" type="email" name="email" id="email" placeholder="Email" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['owner']->email )?>" />
              </div>

      
             </div>
            </div>

           <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
                <label class="control-label">FB Account</label>
                <input style="width = 70%" class="form-control" type="text" name="fbAccount" id="fbAccount" placeholder="Fb Account"  autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['owner']->fbAccount )?>" />
              </div>

      
             </div>
            </div>

          <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">First Name</label>
                <input style="width = 70%" class="form-control" type="text" name="fname" id="fname" placeholder="First Name" \autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['owner']->fname )?>" />
              </div>
             </div>
            </div>

             <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">Middle</label>
                <input style="width = 70%" class="form-control" type="text" name="mname" id="mname" placeholder="Middle Name" \autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['owner']->mname )?>" />
              </div>


             </div>
            </div>

            <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">Last Name</label>
                <input class="form-control" type="text" name="lname" id="lname" placeholder="Last Name" autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['owner']->lname )?>" />
              </div>
             </div>
            </div>

             <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">About Me</label>
                <textarea class="form-control" type="text" name="aboutme" id="aboutme" placeholder="About me" autofocus maxlength="255"rows="10" /> <?php echo htmlspecialchars( $results['owner']->aboutme )?>
                </textarea>
              </div>
             </div>
            </div>

 
         <div class="col-lg-6 col-lg-offset-3 selectContainer" align = "right" class="buttons" >
          <input class="btn btn-primary" type="submit" name="saveChanges" value="Save Changes" />
           <a class="btn btn-danger" href="resto.php?action=restoDashboard">Back to Main Dashboard</a><br><br><hr>
        </div>
 
      </form>
 


<?php include "templates/include/footer.php" ?>