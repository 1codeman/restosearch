<?php include "templates/include/header.php" ?>
<?php include "templates/include/resto.php" ?>
 
      <h1>Resto Table</h1>

      <div align = "right">
  <a class="btn btn-default" href="resto.php?action=restoDashboard">Manage Resto </a>
   <a class="btn btn-default" href="resto.php?action=restoDashboard">Manage Menu </a>
</div>

<hr>
 <?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="alert alert-danger">
                      <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                 </div>
<?php } ?>
<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>
 <table id="mytable" class="table table-bordred table-striped">
                   
           <thead>
             <th>Table Name</th>
             <th>Table Description</th>
            <th>Capacity</th>
              <th>Actions</th>
           </thead>
           <tbody>
    <?php foreach ( $results['restotables'] as $table  ) { ?>
          <tr>
    
        <td><?php echo $table->table_name?></td>
        <td><?php echo $table->table_description?></td>
        <td><?php echo $table->capacity?></td>
         
         
        <td>
        <a class="btn btn-primary btn-xs" href="resto.php?action=editRestoTable&amp;tableId=<?php echo $table->id?>"><span class="glyphicon glyphicon-pencil"></span></a>
        <a class="btn btn-danger btn-xs" href="resto.php?action=deleteRestoTable&amp;tableId=<?php echo $table->id?>"><span class="glyphicon glyphicon-trash"></span></a>
      
    
        </tr>
    
  </tbody>
    <?php } ?>
     <p style="text-align: right">
        <span class="glyphicon glyphicon-list"></span>
                      <?php echo $results['totalRows']?><?php echo ( $results['totalRows'] != 1 ) ? ' tables' : ' table' ?> in total.
                 </p>

</table>

<div align="right">
 <a class="btn btn-default" href="resto.php?action=newRestoTable"><span class="glyphicon glyphicon-plus"></span> New Resto Table </a>
</div>

<?php
// paging buttons will be here
         include_once 'pagingRestoTable.php'; 

?>
<?php include "templates/include/footer.php" ?>