<?php include "templates/include/header.php" ?>
<?php include "templates/include/resto.php" ?>

 <h1><?php echo $results['menu']->menu_name?><small>[<?php echo $results['menu']->menu_description?>]</small></h1>
 

<div align = "right">
      <a class="btn btn-default" href="resto.php?action=listArticles">Manage Article </a>
  <a class="btn btn-default" href="resto.php?action=restoDashboard">Manage Resto </a>
    <a class="btn btn-default" href="resto.php?action=listRestoTable">Manage Table </a>
   <a class="btn btn-default" href="resto.php?action=listMenuItemCategories">Manage Category </a>
   <a class="btn btn-default" href="resto.php?action=editOwnerProfile">Owner Profile </a>
   <a class="btn btn-default" href="resto.php?action=editRestoProfile">Resto Profile </a>
</div>
<hr>
 <?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="alert alert-danger">
                      <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                 </div>
<?php } ?>
<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>
  <table id="mytable" class="table table-bordred table-striped">
                   
           <thead>
            <th>Image</th>
             <th>Menu Item Name</th>
             <th>Description</th>
              <th>Price</th>
              <th>Actions</th>
           </thead>
           <tbody>
    <?php foreach ( $results['menuitems'] as $item  ) { ?>
          <tr>
          
          <?php if ( $item && $imagePath = $item->getImagePath() ) { ?>
          <td>    
          <img class="img-circle" id="itemImage" src="<?php echo $imagePath ?>" alt="Menu Item Image" width ="120" height = "120"/>
         </td>
      

      <?php } else{?>      
                     <td> 
                <img class="img-circle" id="itemImage" src="http://placehold.it/120?text=NULL" alt="MenuItem Image" width ="120" height = "120"/>
               </td>
     <?php } ?>     

        <td><?php echo $item->name?></td>
        <td><?php echo $item->description?></td>
          <td><?php echo $item->price?></td>
         <!--  <td> <img class="img-circle" id="restoImage" src="http://placehold.it/150?text=NULL" alt="Resto Image" width ="150" height = "150"/></td> -->
        <td>
        <a class="btn btn-primary btn-xs" href="resto.php?action=editMenuItem&amp;itemId=<?php echo $item->id?>"><span class="glyphicon glyphicon-pencil"></span></a>
        <a class="btn btn-danger btn-xs" href="resto.php?action=deleteMenuItem&amp;itemId=<?php echo $item->id?>"><span class="glyphicon glyphicon-trash"></span></a>
      
    
        </tr>
    
  </tbody>
    <?php } ?>
     <p style="text-align: right">
        <span class="glyphicon glyphicon-list"></span>
                      <?php echo $results['totalRows']?><?php echo ( $results['totalRows'] != 1 ) ? ' menu items' : ' menu item' ?> in total.
                 </p>

</table>
   
   <!--   <p style="text-align: right">
        <span class="glyphicon glyphicon-list"></span>
                      <?php echo $results['totalRows']?><?php echo ( $results['totalRows'] != 1 ) ? ' items' : ' item' ?> in total.
                 </p>
 -->
</table>

<div align = "right">
  <a class="btn btn-default" href="resto.php?action=newMenuItem"><span class="glyphicon glyphicon-plus"></span>New Menu Item</a>
</div>

      


   
<?php
// paging buttons will be here
         include_once 'pagingMenuItem.php'; 

?>


<?php include "templates/include/footer.php" ?>

