<?php include "templates/include/header.php" ?>
        <script>
$(document).on('ready', function(){
    $('.input-3').rating({displayOnly: true, step: 0.5});
});
</script>

            <!-- Blog Entries Column -->
          
            <h1 class="page-header logo">
                 Must Visit!
                    <small> Restaurant</small>
                </h1>
<center>
           <?php foreach ( $results['restos'] as $resto ) { ?>


          <div class="col-lg-4 col-md-6 col-xs-6 thumb">

             <?php if ( $resto && $imagePath = $resto->getImagePath() ) { ?>
         <a class="" href=".?action=viewResto&amp;restoId=<?php echo $resto->id?>">
          <img class="img-rounded" id="itemImage" src="<?php echo $imagePath ?>" alt="Menu Item Image" width ="200" height = "200"/>
          </a>
      

      <?php } else{?>      
                      <a class="" href=".?action=viewResto&amp;restoId=<?php echo $resto->id?>">
                <img class="img-rounded" id="itemImage" src="http://placehold.it/120?text=NULL" alt="MenuItem Image" width ="200" height = "200"/>
              </a>
     <?php } ?>     



              <div class="caption">
              <h4>
                <a href=".?action=viewResto&amp;restoId=<?php echo $resto->id?>"><?php echo htmlspecialchars( $resto->resto_name )?></a>
                   </h4>
                    <p class="summary"><?php echo htmlspecialchars( $resto->resto_description )?></p>
                    <div align="right">
                  <!--  <a class="btn btn-info btn-s" href=".?action=viewResto&amp;restoId=<?php echo $resto->id?>"><span class="glyphicon glyphicon-chevron-right"></span> Read More </a> -->
                    </div> 
                    
                    <?php 
                                        
                       //get feedbacks
                      $data = RestoFeedback::getList($_SESSION['userId'],$resto->id,0,1000000);
                      $results['restofeedbacks'] = $data['results'];
                      $results['totalRatingRows'] = $data['totalRows'];
                      $results['totalRatings'] = $data['totalRatings'];
                    
                    ?>
                    
                    
                    
                    
                     <?php if($results['totalRatingRows'] !=  0){  ?>
  <input class="input-3" value="<?php echo round($results['totalRatings'] / $results['totalRatingRows'],2) ?>" class="rating-loading" data-size="sm">
  
     <?php  }else{ ?>
        
          <input class="input-3" value="0" class="rating-loading" data-size="sm">
       <?php  }?>
                     
                     
                      <b> <p>(<?php if($results['totalRatingRows'] !=  0){
               echo round($results['totalRatings'] / $results['totalRatingRows'],2)."/5)";
               
               }else{
               echo "no feedback yet)";
               }?></p></b> <tr>
   
      


                    <hr>
            </div>
            
          </div>      

        <?php } ?>
        

    
      <!-- <div class="container">      
          <a class="btn btn-default" href="./?action=archive"><span class="glyphicon glyphicon-folder-close"></span> Resto Archive </a>
         </div> -->
<br>




<?php include "templates/include/footer.php" ?> 