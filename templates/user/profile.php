<?php include "templates/include/header.php" ?>


          
             <script>
 
      // Prevents file upload hangs in Mac Safari
      // Inspired by http://airbladesoftware.com/notes/note-to-self-prevent-uploads-hanging-in-safari
 
      function closeKeepAlive() {
        if ( /AppleWebKit|MSIE/.test( navigator.userAgent) ) {
          var xhr = new XMLHttpRequest();
          xhr.open( "GET", "/ping/close", false );
          xhr.send();
        }
      }
 
      </script>
 
      <center> <h1 class="page-header logo">

                 <?php echo $_SESSION['user-username'] . "'s";?>

                    <small>profile</small>
            </h1></center>

           
       
  

      <form action="index.php?action=<?php echo $results['formAction']?>" method="post" align="center" enctype="multipart/form-data" onsubmit="closeKeepAlive()">
        <input type="hidden" name="uname" value="<?php echo $results['user']->uname ?>"/>
 
<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>


<?php if ( $results['user'] && $imagePath = $results['user']->getImagePath() ) { ?>
      <div class="form-group ">
            <img class="img-circle" id="ownerImage" src="<?php echo $imagePath ?>" alt="User Image" width ="150" height = "150"/>
         
        <div class="form-group ">
            <input type="checkbox" name="deleteImage" id="deleteImage" value="yes"/ > <label for="deleteImage">Delete</label>
    
          </div>
    </div>

      <?php } else{?>       
                <img class="img-circle" id="ownerImage" src="http://placehold.it/150?text=NULL" alt="User Image" width ="150" height = "150"/>
     <?php } ?>     
      <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
            <input type="file" name="image" id="image" placeholder="Choose an image to upload" maxlength="255" />
         
          </div>

      
             </div>
            </div>



          <div class="form-group ">
             <div class="row " >
                <div class="col-lg-6 col-lg-offset-3 selectContainer ">
                <label class="control-label">Email</label>
                <input style="width = 70%" class="form-control" type="email" name="email" id="email" placeholder="Email" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['user']->email )?>" />
              </div>

      
             </div>
            </div>


          <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">First Name</label>
                <input style="width = 70%" class="form-control" type="text" name="fname" id="fname" placeholder="First Name" \autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['user']->fname )?>" />
              </div>
             </div>
            </div>

           
            <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">Last Name</label>
                <input class="form-control" type="text" name="lname" id="lname" placeholder="Last Name" autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['user']->lname )?>" />
              </div>
             </div>
            </div>

             <div class="form-group">
             <div class="row">
                <div class="col-lg-6 col-lg-offset-3 selectContainer">
                <label class="control-label">About Me</label>
                <textarea class="form-control" type="text" name="aboutme" id="aboutme" placeholder="About me" autofocus maxlength="255"rows="10" /> <?php echo htmlspecialchars( $results['user']->aboutme )?>
                </textarea>
              </div>
             </div>
            </div>

 
         <div class="col-lg-6 col-lg-offset-3 selectContainer" align = "right" class="buttons" >
          <input class="btn btn-primary" type="submit" name="saveChanges" value="Save Changes" />
           <a class="btn btn-danger" href=".">Back to Home Page</a><br><br><hr>
        </div>
 
      </form>
 


<?php include "templates/include/footer.php" ?>