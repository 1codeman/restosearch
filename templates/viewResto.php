<?php include "templates/include/header.php" ?>
        <script>
$(document).on('ready', function(){
    $('.input-3').rating({displayOnly: true, step: 0.5});
});
</script>

    
</div>

    <!-- Heading Row -->
        <div class="row">
    <center>
            <div class="col-md-4">
               <?php if (  $results['resto'] && $imagePath =  $results['resto']->getImagePath() ) { ?>
         <a class="" href=".?action=viewResto&amp;restoId=<?php echo $resto->id?>">
          <img class="img-circle" id="itemImage" src="<?php echo $imagePath ?>" alt="Menu Item Image" width ="200" height = "200"/>
          </a>
      

      <?php } else{?>      
                      <a class="" href=".?action=viewResto&amp;restoId=<?php echo $resto->id?>">
                <img class="img-circle" id="itemImage" src="http://placehold.it/120?text=NULL" alt="MenuItem Image" width ="200" height = "200"/>
              </a>
     <?php } ?>     
    <h1 class="page-header" style="width: 75%;"><?php echo htmlspecialchars( $results['resto']->resto_name )?></h1>
    
    
    <?php if($results['totalRatingRows'] !=  0){  ?>
  <input class="input-3" value="<?php echo round($results['totalRatings'] / $results['totalRatingRows'],2) ?>" class="rating-loading" data-size="sm">
  
     <?php  }else{ ?>
        
          <input class="input-3" value="0" class="rating-loading" data-size="sm">
       <?php  }?>
    
  
 <b> <p>(<?php if($results['totalRatingRows'] !=  0){
               echo round($results['totalRatings'] / $results['totalRatingRows'],2)."/5)";
               
               }else{
               echo "no feedback yet)";
               }?></p></b>

            </div>
            
            <!-- /.col-md-4 -->
                    
            <!-- /.col-md-8 -->
            <div class="col-md-8">
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!--<ol class="carousel-indicators">-->
  <!--  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
  <!--  <li data-target="#myCarousel" data-slide-to="1"></li>-->
  <!--  <li data-target="#myCarousel" data-slide-to="2"></li>-->
  <!--  <li data-target="#myCarousel" data-slide-to="3"></li>-->
  <!--</ol>-->

  <!-- Wrapper for slides -->
 <?php if (  $results['resto'] && $imagePath =  $results['resto']->getImagePath2() ) { ?>
        
          <img class="" id="itemImage" src="<?php echo $imagePath ?>" alt="Cover Photo" width ="750" height = "350"/>
        
      

      <?php } else{?>      
         
                <img class="" id="itemImage" src="http://placehold.it/750X350?text=COVER PHOTO" alt="MenuItem Image" width ="750" height = "350"/>
              
     <?php } ?>     
    
 


  <!-- Left and right controls -->
  <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">-->
  <!--  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
  <!--  <span class="sr-only">Previous</span>-->
  <!--</a>-->
  <!--<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">-->
  <!--  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
  <!--  <span class="sr-only">Next</span>-->
  <!--</a>-->
</div>


<br>
 <?php foreach ( $results['3menuitems'] as $item  ) { ?>
    <div class="col-lg-4">     
          <center> <h2> <?php echo $item->name?></h2>
          <?php if ( $item && $imagePath = $item->getImagePath() ) { ?>
          <td>    
          <img class="img-circle" id="itemImage" src="<?php echo $imagePath ?>" alt="Menu Item Image" width ="120" height = "120"/>
         </td>
      

      <?php } else{?>      
                     <td> 
                <img class="img-circle" id="itemImage" src="http://placehold.it/120?text=NULL" alt="MenuItem Image" width ="120" height = "120"/>
               </td>
     <?php } ?>     
        
          <p><?php echo $item->description?></p>
           <p>(<?php echo $item->price?>)</p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
          </center>
        </div><!-- /.col-lg-4 -->
               
    <?php } ?>
               
               
            </div>
        </div>
        <!-- /.row -->
  <!-- Content Row -->
      
<hr>

        <!-- Call to Action Well -->
        <div class="row">
            <div class="col-lg-12">
                <div class="well text-center">
                    <?php echo $results['resto']->resto_description?>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

            <div class="col-lg-12">
                <div id='map'  style="width:100%; height:300px;"></div>
                <hr>
              </div>

<hr>

      <!-- Projects Row -->
        <div class="row">
             <?php foreach ( $results['articles'] as $item  ) { ?>
            <div class="col-md-4 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x400?text=Article" alt="">
                </a>
                <h3>
                    <a href="#"> <?php echo $item->title?></a>
                </h3>
                <p> <?php echo $item->summary?></p>
            </div>
            <?php } ?>
          
        </div>
        <!-- /.row -->

<hr>
<div class="detailBox">
    <div class="titleBox">
      <label>Feedbacks (<?php echo $results['totalRatingRows'] ?>)</label>
       
    </div>
    <div class="actionBox">
        <ul class="commentList">
             <?php foreach ( $results['restofeedbacks'] as $restofeedback ) { ?>  
            
                    <li>
                       
                        <div class="commenterImage">
                         <?php if (  $restofeedback && $imagePath =  $restofeedback->getImagePath() ) { ?>

                            <img src="<?php echo $imagePath ?>"/>
         

      <?php } else{?>       
                <img src="http://placehold.it/120?text=NULL" />
     <?php } ?>     
                        </div>
                        <div class="commentText">
                              <b> <p class=""><?php echo $restofeedback->uname?></p></b>
                            <p class=""><?php echo $restofeedback->feedback?></p> <span class="date sub-text"><?php echo date('j F Y H:i:s', strtotime($restofeedback->publicationDate))?> </span>
          <input class="input-3" value="<?php echo $restofeedback->rating?>" class="rating-loading" data-size="xs">
                        </div>
                    </li>
            <?php } ?>    
            
            
            
            
        </ul>
  
        <!--<form class="form-inline" role="form">-->
        <!--    <div class="form-group">-->
        <!--        <input class="form-control" type="text" placeholder="Your comments" />-->
        <!--    </div>-->
        <!--    <div class="form-group">-->
        <!--        <button class="btn btn-default">Add</button>-->
        <!--    </div>-->
        <!--</form>-->
    </div>
</div>
<div class="detailBox">
    
                      
    <div class="titleBox">

         <?php if(isset($_SESSION['user-username'])){ ?>
                  <div class="form-group">
               <label for="">Cast Your Feedbacks</label>
      <form action="index.php?action=castFeedback" method="post">
               <center>
    <input id="input-5" class="rating-loading" data-show-clear="false" name="rating" data-show-caption="false" value="3">
    
    
    </center>
    <script>
    $(document).on('ready', function(){
        $('#input-5').rating({clearCaption: 'No stars yet'});
    });
    </script>
                    <div class="form-group">
      <textarea class="form-control" rows="5" id="comment" name="feedback" required autofocus maxlength="255"></textarea>
      
    </div>
    <div align="right">
        <input class="btn btn-primary" type="submit" name="saveChanges" value="Cast Feedback" />
    </div>
    
    
</form>


</div>
      <?php }?>
      
         <?php if(!isset($_SESSION['user-username'])){ ?>
         <center>
              <p><a class="btn btn-lg btn-danger" href=".?action=user-login"><span class="glyphicon glyphicon-user"></span>Login to Rate Us!</a></p>
         </center>
            
      <?php }?>
      
      


 
    </div>
</div>



<hr><br>

<?php if( $results['totalRows'] > 0 ){ ?>
<h1><?php echo htmlspecialchars( $results['menu']->menu_name )?></h1>
 <?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="alert alert-danger">
                      <i class="glyphicon glyphicon-remove-sign"></i> &nbsp;<?php echo $results['errorMessage'] ?>
                 </div>
<?php } ?>
<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="alert alert-info">
                      <i class="glyphicon glyphicon-thumbs-up"></i> &nbsp;<?php echo $results['statusMessage'] ?>
                 </div>
<?php } ?>

  <table id="mytable" class="table table-bordred table-striped">
                   
           <thead>
            <th>Image</th>
             <th>Menu Item Name</th>
             <th>Description</th>
              <th>Price</th>
            
           </thead>
           <tbody>
    <?php foreach ( $results['menuitems'] as $item  ) { ?>
          <tr>
          
          <?php if ( $item && $imagePath = $item->getImagePath() ) { ?>
          <td>    
          <img class="img-circle" id="itemImage" src="<?php echo $imagePath ?>" alt="Menu Item Image" width ="120" height = "120"/>
         </td>
      

      <?php } else{?>      
                     <td> 
                <img class="img-circle" id="itemImage" src="http://placehold.it/120?text=NULL" alt="MenuItem Image" width ="120" height = "120"/>
               </td>
     <?php } ?>     

        <td><?php echo $item->name?></td>
        <td><?php echo $item->description?></td>
          <td><?php echo $item->price?></td>
         <!--  <td> <img class="img-circle" id="restoImage" src="http://placehold.it/150?text=NULL" alt="Resto Image" width ="150" height = "150"/></td> -->
      
    
        </tr>
    
  </tbody>
    <?php } ?>
     <p style="text-align: right">
        <span class="glyphicon glyphicon-list"></span>
                      <?php echo $results['totalRows']?><?php echo ( $results['totalRows'] != 1 ) ? ' menu items' : ' menu item' ?> in total.
                 </p>

</table>
   
   <!--   <p style="text-align: right">
        <span class="glyphicon glyphicon-list"></span>
                      <?php echo $results['totalRows']?><?php echo ( $results['totalRows'] != 1 ) ? ' items' : ' item' ?> in total.
                 </p>
 -->
</table>


      
<?php } ?>


</div>
<?php include "templates/include/footer.php" ?>

