<?php
 
/**
 * Class to handle user
 */
 
class Owner
{
  // Properties
 
  /**
  * @var int The owner ID from the database
  */
  public $id = null;
 
  /**
  * @var string userName of the owner
  */
  public $uname = null;
 
  /**
  * @var string email of the owner
  */
  public $email = null;

   /**
  * @var string password of the owner
  */
  public $password = null;
  
  /**
  * @var string first name of the owner
  */
  public $fname = null;

   /**
  * @var string middle name of the owner
  */
  public $mname = null;


  /**
  * @var string last name of the owner
  */
  public $lname = null;



  /**
  * @var string A short description of the owner
  */
  public $aboutme = null;


 /**
  * @var string Fb account of the owner
  */
  public $fbAccount = null;


/**
  * @var string The filename extension of the owners's full-size and thumbnail images (empty string means the owner has no image)
  */
  public $imageExtension = "";


    /**
  * @var int The owner resto id
  */
  public $restoId = null;

 
  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['uname'] ) ) $this->uname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['uname'] );
    if ( isset( $data['email'] ) ) $this->email = $data['email'];
    if ( isset( $data['password'] ) ) $this->password = $data['password'];
    if ( isset( $data['fname'] ) ) $this->fname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['fname'] );
    if ( isset( $data['mname'] ) ) $this->mname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['mname'] );
    if ( isset( $data['lname'] ) ) $this->lname = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['lname'] );
    if ( isset( $data['aboutme'] ) ) $this->aboutme = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['aboutme'] );
    if ( isset( $data['imageExtension'] ) ) $this->imageExtension = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\$ a-zA-Z0-9()]/", "", $data['imageExtension'] );
    if ( isset( $data['fbAccount'] ) ) $this->fbAccount = $data['fbAccount'];
  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) {
 
    // Store all the parameters
    $this->__construct( $params );
  }
 
 
  /**
  * Returns a Owner object matching the given owner ID
  *
  * @param string The username
  * @return user|false The user object, or false if the record was not found or there was a problem
  */
 
  public static function getByUsername( $uname ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM owners WHERE uname = :uname";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":uname", $uname, PDO::PARAM_STR);
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) return new Owner( $row );
  }
 
 
  /**
  * Returns all (or a range of) Owner objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
   *@param int Optional The number of rows per page to return (default=all)
  * @param string Optional column by which to order the categories (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Category objects; totalRows => Total number of categories
  */
 
  public static function getList( $numRows=0, $perPage=10000000,$order="id ASC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM owners
            ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $owner = new Owner( $row );
      $list[] = $owner;
    }
 
    // Now get the total number of owner that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }
 
 
  /**
  * Inserts the current Owner object into the database, and sets its ID property.
  */
 
  public function insert($restoId) {
 
    // Does the User Owner object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "User::insert(): Attempt to insert a User object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Owner
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO owners ( uname,email,password,restoId) VALUES ( :uname, :email,:password,:restoId)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":uname", $this->uname, PDO::PARAM_STR );
    $st->bindValue( ":email", $this->email, PDO::PARAM_STR );
    $st->bindValue( ":password", sha1($this->password), PDO::PARAM_STR );
    $st->bindValue( ":restoId", $restoId, PDO::PARAM_INT );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
    return $this->id;
  }
 
 
  /**
  * Updates the current Owner object in the database.
  */
 
  public function update() {
 
    // Does the Owner object have an ID?
    if ( is_null( $this->uname ) ) trigger_error ( "Owner::update(): Attempt to update a User object that does not have its ID property set.", E_USER_ERROR );
    
    // Update the Owner
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE owners SET email=:email , fname=:fname , mname=:mname , lname=:lname , aboutme=:aboutme,imageExtension = :imageExtension  , fbAccount=:fbAccount  WHERE uname = :uname";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":uname", $this->uname, PDO::PARAM_STR );
    $st->bindValue( ":email", $this->email, PDO::PARAM_STR );
    $st->bindValue( ":fname", $this->fname, PDO::PARAM_STR );
    $st->bindValue( ":mname", $this->mname, PDO::PARAM_STR );
    $st->bindValue( ":lname",  $this->lname, PDO::PARAM_STR );
    $st->bindValue( ":aboutme",  $this->aboutme, PDO::PARAM_STR );
    $st->bindValue( ":imageExtension", $this->imageExtension, PDO::PARAM_STR );
    $st->bindValue( ":fbAccount",  $this->fbAccount, PDO::PARAM_STR );
    $st->execute();
    $conn = null;
  }
 
 
  // /**
  // * Deletes the current Category object from the database.
  // */
 
  // public function delete() {
 
  //   // Does the Category object have an ID?
  //   if ( is_null( $this->id ) ) trigger_error ( "Category::delete(): Attempt to delete a Category object that does not have its ID property set.", E_USER_ERROR );
 
  //   // Delete the Category
  //   $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
  //   $st = $conn->prepare ( "DELETE FROM categories WHERE id = :id LIMIT 1" );
  //   $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
  //   $st->execute();
  //   $conn = null;
  // }
 



 /**
  * Stores any image uploaded from the edit form
  *
  * @param assoc The 'image' element from the $_FILES array containing the file upload data
  */
 
  public function storeUploadedImage( $image ) {
 
    if ( $image['error'] == UPLOAD_ERR_OK )
    {
      // Does the Owner object have an ID?
      if ( is_null( $this->id ) ) trigger_error( "Article::storeUploadedImage(): Attempt to upload an image for an Owner object that does not have its ID property set.", E_USER_ERROR );
 
      // Delete any previous image(s) for this article
      $this->deleteImages();
 
      // Get and store the image filename extension
      $this->imageExtension = strtolower( strrchr( $image['name'], '.' ) );
 
      // Store the image
 
      $tempFilename = trim( $image['tmp_name'] ); 
 
      if ( is_uploaded_file ( $tempFilename ) ) {
        if ( !( move_uploaded_file( $tempFilename, $this->getImagePath() ) ) ) trigger_error( "Owner::storeUploadedImage(): Couldn't move uploaded file.", E_USER_ERROR );
        if ( !( chmod( $this->getImagePath(), 0666 ) ) ) trigger_error( "Owner::storeUploadedImage(): Couldn't set permissions on uploaded file.", E_USER_ERROR );
      }
 
      // Get the image size and type
      $attrs = getimagesize ( $this->getImagePath() );
      $imageWidth = $attrs[0];
      $imageHeight = $attrs[1];
      $imageType = $attrs[2];
 
      // Load the image into memory
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          $imageResource = imagecreatefromgif ( $this->getImagePath() );
          break;
        case IMAGETYPE_JPEG:
          $imageResource = imagecreatefromjpeg ( $this->getImagePath() );
          break;
        case IMAGETYPE_PNG:
          $imageResource = imagecreatefrompng ( $this->getImagePath() );
          break;
        default:
          trigger_error ( "Owner::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      // Copy and resize the image to create the thumbnail
      $thumbHeight = intval ( $imageHeight / $imageWidth * OWNER_THUMB_WIDTH );
      $thumbResource = imagecreatetruecolor ( OWNER_THUMB_WIDTH, $thumbHeight );
      imagecopyresampled( $thumbResource, $imageResource, 0, 0, 0, 0, OWNER_THUMB_WIDTH, $thumbHeight, $imageWidth, $imageHeight );
 
      // Save the thumbnail
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          imagegif ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        case IMAGETYPE_JPEG:
          imagejpeg ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ), JPEG_QUALITY );
          break;
        case IMAGETYPE_PNG:
          imagepng ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        default:
          trigger_error ( "Article::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      $this->update();
    }
  }
 
 
  /**
  * Deletes any images and/or thumbnails associated with the article
  */
 
  public function deleteImages() {
 
    // Delete all fullsize images for this article
    foreach (glob( OWNER_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Owner::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
    }
     
    // Delete all thumbnail images for this article
    foreach (glob( OWNER_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Owner::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
    }
 
    // Remove the image filename extension from the object
    $this->imageExtension = "";
  }
 
 
  /**
  * Returns the relative path to the article's full-size or thumbnail image
  *
  * @param string The type of image path to retrieve (IMG_TYPE_FULLSIZE or IMG_TYPE_THUMB). Defaults to IMG_TYPE_FULLSIZE.
  * @return string|false The image's path, or false if an image hasn't been uploaded
  */
 
  public function getImagePath( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension ) ? ( OWNER_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension ) : false;
  }
 
 

}
 
?>