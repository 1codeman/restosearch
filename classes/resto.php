<?php
 
/**
 * Class to handle resto
 */
 
class Resto
{
  // Properties
 
  /**
  * @var int The item resto ID from the database
  */
  public $id = null;
 
  /**
  * @var string Name of the resto
  */
  public $resto_name = null;
 
  /**
  * @var string A short description of the resto
  */
  public $description = null;

    /**
  * @var string A contact number of the resto
  */
  public $contactNo = null;

    /**
  * @var string category of the resto
  */
  public $category = null;

    /**
  * @var string Address of the resto
  */
  public $address = null;

    /**
  * @var string longitude coordinate of the resto
  */
  public $lon = null;

    /**
  * @var string lat coordinate of the resto
  */
  public $lat = null;
 
  /**
  * @var string The filename extension of the owners's full-size and thumbnail images (empty string means the owner has no image)
  */
  public $imageExtension = "";
  
  
    /**
  * @var string The filename extension of the owners's full-size and thumbnail images (empty string means the owner has no image)
  */
  public $imageExtension2 = "";

  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['resto_name'] ) ) $this->resto_name = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['resto_name'] );
    if ( isset( $data['resto_description'] ) ) $this->resto_description = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['resto_description'] );
    if ( isset( $data['contactNo'] ) ) $this->contactNo = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['contactNo'] );
    if ( isset( $data['category'] ) ) $this->category = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['category'] );
    if ( isset( $data['address'] ) ) $this->address = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['address'] );
    if ( isset( $data['lon'] ) ) $this->lon = (float) $data['lon'];
    if ( isset( $data['lat'] ) ) $this->lat = (float) $data['lat'];
   if ( isset( $data['imageExtension'] ) ) $this->imageExtension = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\$ a-zA-Z0-9()]/", "", $data['imageExtension'] );
   if ( isset( $data['imageExtension2'] ) ) $this->imageExtension2 = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\$ a-zA-Z0-9()]/", "", $data['imageExtension2'] );
  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) {
 
    // Store all the parameters
    $this->__construct( $params );
  }
 
 
  /**
  * Returns a item Category object matching the given item resto ID
  *
  * @param int The item resto ID
  * @return Resto|false The item resto object, or false if the record was not found or there was a problem
  */
 
  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM restos WHERE id = :id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id",$id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null;

    if ( $row ) return new Resto( $row );
  }
 
 
  /**
  * Returns all (or a range of) item Category objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param string Optional column by which to order the resto (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Category objects; totalRows => Total number of categories
  */
 
  public static function getList($numRows=0, $perPage=10000000,$order="resto_name ASC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM restos
            ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $resto = new Resto( $row );
      $list[] = $resto;
    }
 
    // Now get the total number of restos that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }
 
 
  /**
  * Inserts the current restos object into the database, and sets its ID property.
  */
 
  public function insert() {
 
    // Does the Resto object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "Resto::insert(): Attempt to insert a resto object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Item Category
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO restos ( resto_name, resto_description) VALUES ( :resto_name, :resto_description)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":resto_name", $this->resto_name, PDO::PARAM_STR );
    $st->bindValue( ":resto_description", $this->resto_description, PDO::PARAM_STR );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
    return $this->id;
  }
 
 
  /**
  * Updates the current resto object in the database.
  */
 
  public function update() {
     
    // Does the Resto object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Resto::update(): Attempt to update a Category object that does not have its ID property set.", E_USER_ERROR );
   
    // Update the Category
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE  restos SET resto_name=:resto_name, resto_description=:resto_description , contactNo=:contactNo , category=:category,address=:address,lon=:lon,lat=:lat,imageExtension = :imageExtension ,imageExtension2 = :imageExtension2  WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":resto_name", $this->resto_name, PDO::PARAM_STR );
    $st->bindValue( ":resto_description", $this->resto_description, PDO::PARAM_STR );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->bindValue( ":contactNo", $this->contactNo, PDO::PARAM_STR );
    $st->bindValue( ":category", $this->category, PDO::PARAM_STR );
    $st->bindValue( ":address", $this->address, PDO::PARAM_STR );
    $st->bindValue( ":lon", $this->lon, PDO::PARAM_STR );
    $st->bindValue( ":lat", $this->lat, PDO::PARAM_STR );
    $st->bindValue( ":imageExtension", $this->imageExtension, PDO::PARAM_STR );
    $st->bindValue( ":imageExtension2", $this->imageExtension2, PDO::PARAM_STR );
    $st->execute();
    $conn = null;
  }
 
 
  /**
  * Deletes the current item Resto object from the database.
  */
 
  public function delete() {
 
    // Does the Resto object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Resto::delete(): Attempt to delete a Category object that does not have its ID property set.", E_USER_ERROR );
 
    // Delete the Resto
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM restos WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 




 /**
  * Stores any image uploaded from the edit form
  *
  * @param assoc The 'image' element from the $_FILES array containing the file upload data
  */
 
  public function storeUploadedImage( $image ) {
 
    if ( $image['error'] == UPLOAD_ERR_OK )
    {
      // Does the Resto object have an ID?
      if ( is_null( $this->id ) ) trigger_error( "Resto:storeUploadedImage(): Attempt to upload an image for an Resto object that does not have its ID property set.", E_USER_ERROR );
 
      // Delete any previous image(s) for this article
      $this->deleteImages();
 
      // Get and store the image filename extension
      $this->imageExtension = strtolower( strrchr( $image['name'], '.' ) );
 
      // Store the image
 
      $tempFilename = trim( $image['tmp_name'] ); 
 
      if ( is_uploaded_file ( $tempFilename ) ) {
        if ( !( move_uploaded_file( $tempFilename, $this->getImagePath() ) ) ) trigger_error( "Resto::storeUploadedImage(): Couldn't move uploaded file.", E_USER_ERROR );
        if ( !( chmod( $this->getImagePath(), 0666 ) ) ) trigger_error( "Resto::storeUploadedImage(): Couldn't set permissions on uploaded file.", E_USER_ERROR );
      }
 
      // Get the image size and type
      $attrs = getimagesize ( $this->getImagePath() );
      $imageWidth = $attrs[0];
      $imageHeight = $attrs[1];
      $imageType = $attrs[2];
 
      // Load the image into memory
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          $imageResource = imagecreatefromgif ( $this->getImagePath() );
          break;
        case IMAGETYPE_JPEG:
          $imageResource = imagecreatefromjpeg ( $this->getImagePath() );
          break;
        case IMAGETYPE_PNG:
          $imageResource = imagecreatefrompng ( $this->getImagePath() );
          break;
        default:
          trigger_error ( "Resto::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      // Copy and resize the image to create the thumbnail
      $thumbHeight = intval ( $imageHeight / $imageWidth * RESTO_THUMB_WIDTH );
      $thumbResource = imagecreatetruecolor ( RESTO_THUMB_WIDTH, $thumbHeight );
      imagecopyresampled( $thumbResource, $imageResource, 0, 0, 0, 0, RESTO_THUMB_WIDTH, $thumbHeight, $imageWidth, $imageHeight );
 
      // Save the thumbnail
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          imagegif ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        case IMAGETYPE_JPEG:
          imagejpeg ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ), JPEG_QUALITY );
          break;
        case IMAGETYPE_PNG:
          imagepng ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        default:
          trigger_error ( "Resto::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      $this->update();
    }
  }
 
 
  /**
  * Deletes any images and/or thumbnails associated with the article
  */
 
  public function deleteImages() {
 
    // Delete all fullsize images for this article
    foreach (glob( RESTO_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Resto::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
    }
     
    // Delete all thumbnail images for this article
    foreach (glob( RESTO_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Resto::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
    }
 
    // Remove the image filename extension from the object
    $this->imageExtension = "";
  }
 
 /**
  * Stores any image uploaded from the edit form
  *
  * @param assoc The 'image' element from the $_FILES array containing the file upload data
  */
 
  public function storeUploadedImage2( $image ) {
 
    if ( $image['error'] == UPLOAD_ERR_OK )
    {
      // Does the Resto object have an ID?
      if ( is_null( $this->id ) ) trigger_error( "Resto:storeUploadedImage(): Attempt to upload an image for an Resto object that does not have its ID property set.", E_USER_ERROR );
 
      // Delete any previous image(s) for this article
      $this->deleteImages2();
 
      // Get and store the image filename extension
      $this->imageExtension2 = strtolower( strrchr( $image['name'], '.' ) );
 
      // Store the image
 
      $tempFilename = trim( $image['tmp_name'] ); 
 
      if ( is_uploaded_file ( $tempFilename ) ) {
        if ( !( move_uploaded_file( $tempFilename, $this->getImagePath2() ) ) ) trigger_error( "Resto::storeUploadedImage(): Couldn't move uploaded file.", E_USER_ERROR );
        if ( !( chmod( $this->getImagePath2(), 0666 ) ) ) trigger_error( "Resto::storeUploadedImage(): Couldn't set permissions on uploaded file.", E_USER_ERROR );
      }
 
      // Get the image size and type
      $attrs = getimagesize ( $this->getImagePath2() );
      $imageWidth = $attrs[0];
      $imageHeight = $attrs[1];
      $imageType = $attrs[2];
 
      // Load the image into memory
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          $imageResource = imagecreatefromgif ( $this->getImagePath2() );
          break;
        case IMAGETYPE_JPEG:
          $imageResource = imagecreatefromjpeg ( $this->getImagePath2() );
          break;
        case IMAGETYPE_PNG:
          $imageResource = imagecreatefrompng ( $this->getImagePath2() );
          break;
        default:
          trigger_error ( "Resto::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      // Copy and resize the image to create the thumbnail
      $thumbHeight = intval ( $imageHeight / $imageWidth * COVERPHOTO_THUMB_WIDTH );
      $thumbResource = imagecreatetruecolor ( COVERPHOTO_THUMB_WIDTH, $thumbHeight );
      imagecopyresampled( $thumbResource, $imageResource, 0, 0, 0, 0, COVERPHOTO_THUMB_WIDTH, $thumbHeight, $imageWidth, $imageHeight );
 
      // Save the thumbnail
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          imagegif ( $thumbResource, $this->getImagePath2( IMG_TYPE_THUMB ) );
          break;
        case IMAGETYPE_JPEG:
          imagejpeg ( $thumbResource, $this->getImagePath2( IMG_TYPE_THUMB ), JPEG_QUALITY );
          break;
        case IMAGETYPE_PNG:
          imagepng ( $thumbResource, $this->getImagePath2( IMG_TYPE_THUMB ) );
          break;
        default:
          trigger_error ( "Resto::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      $this->update();
    }
  }
 
 
  /**
  * Deletes any images and/or thumbnails associated with the article
  */
 
  // public function deleteImages() {
 
  //   // Delete all fullsize images for this article
  //   foreach (glob( RESTO_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
  //     if ( !unlink( $filename ) ) trigger_error( "Resto::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
  //   }
     
  //   // Delete all thumbnail images for this article
  //   foreach (glob( RESTO_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
  //     if ( !unlink( $filename ) ) trigger_error( "Resto::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
  //   }
 
  //   // Remove the image filename extension from the object
  //   $this->imageExtension = "";
  // }
 
 
  /**
  * Deletes any images and/or thumbnails associated with the article
  */
 
  public function deleteImages2() {
 
    // Delete all fullsize images for this article
    foreach (glob( COVERPHOTO_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Resto::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
    }
     
    // Delete all thumbnail images for this article
    foreach (glob( COVERPHOTO_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "Resto::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
    }
 
    // Remove the image filename extension from the object
    $this->imageExtension2 = "";
  }
 
  /**
  * Returns the relative path to the article's full-size or thumbnail image
  *
  * @param string The type of image path to retrieve (IMG_TYPE_FULLSIZE or IMG_TYPE_THUMB). Defaults to IMG_TYPE_FULLSIZE.
  * @return string|false The image's path, or false if an image hasn't been uploaded
  */
 
  public function getImagePath( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension ) ? ( RESTO_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension ) : false;
  }
 
 
 
   /**
  * Returns the relative path to the article's full-size or thumbnail image
  *
  * @param string The type of image path to retrieve (IMG_TYPE_FULLSIZE or IMG_TYPE_THUMB). Defaults to IMG_TYPE_FULLSIZE.
  * @return string|false The image's path, or false if an image hasn't been uploaded
  */
 
  public function getImagePath2( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension2 ) ? ( COVERPHOTO_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension2 ) : false;
  }
 
 
}
 
?>