<?php
 
/**
 * Class to handle menu item
 */
 
class MenuItem
{
 
  // Properties
 
  /**
  * @var int The menu item ID 
  */
  public $id = null;

   /**
  * @var string name of the menu item
  */
  public $name = null;
 
  /**
  * @var string description of the menu item
  */
  public $description = null;
 
  /**
  * @var float price of the item menu
  */
  public $price  = null;


  /**
  * @var int menu item category ID
  */
  public $categoryID = null;


  /**
  * @var int menu item menu ID
  */
  public $menuID = null;
 

  /**
  * @var string The filename extension of the menu item's full-size and thumbnail images (empty string means the article has no image)
  */
  public $imageExtension = "";
 
 
  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['name'] ) ) $this->name = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['name'] );
    if ( isset( $data['description'] ) ) $this->description = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['description'] );
    if ( isset( $data['price'] ) ) $this->price = (float) $data['price'];
    if ( isset( $data['categoryID'] ) ) $this->categoryID = (int) $data['categoryID'];
    if ( isset( $data['menuID'] ) ) $this->menuID = (int) $data['menuID'];
    if ( isset( $data['imageExtension'] ) ) $this->imageExtension = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\$ a-zA-Z0-9()]/", "", $data['imageExtension'] );

  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) { //this function will handle the create and update menu item
 
    // Store all the parameters
    $this->__construct( $params );
 
  }
 
 
  /**
  * Returns an Menu Item object matching the given menu item ID
  *
  * @param int The Menu item ID
  * @return MenuItem|false The menu item object, or false if the record was not found or there was a problem
  */

  //static keyword to the method definitionallows the method to be called directly without specifying an object
 
  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD ); // makes a connection to the MySQL database using the login details from the config.php 
    $sql = "SELECT * FROM menu_items WHERE id = :id"; 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null; //close connection
    if ( $row ) return new MenuItem( $row );
  }
 
 
   /**
  * Returns all (or a range of) Menu Item objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param int Optional Return just articles in the category with this ID
  * @param string Optional column by which to order the articles (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Article objects; totalRows => Total number of menu items
  */
 
  public static function getList( $numRows=0, $perPage=10000000, $menuID=null, $order="RAND()" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM menu_items WHERE menuID = :menuID
            ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->bindValue( ":menuID", $menuID, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $menuitem = new MenuItem( $row );
      $list[] = $menuitem;
    }
 
    // Now get the total number of articles that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }
 
 
  /**
  * Inserts the current Article object into the database, and sets its ID property.
  */
 
  public function insert() {
 
    // Does the Menu ITem object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "MenuItem::insert(): Attempt to insert an MenuItem object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Menu Item
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO menu_items (name,description,price,categoryID,menuID,imageExtension) VALUES (:name, :description, :price, :categoryID, :menuID,:imageExtension)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":name", $this->name, PDO::PARAM_STR );
    $st->bindValue( ":description", $this->description, PDO::PARAM_STR );
    $st->bindValue( ":price", $this->price, PDO::PARAM_STR );
    $st->bindValue( ":categoryID",$this->categoryID, PDO::PARAM_INT );
    $st->bindValue( ":menuID",$this->menuID, PDO::PARAM_INT );
    $st->bindValue( ":imageExtension",$this->imageExtension, PDO::PARAM_STR );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }
 
 
  /**
  * Updates the current Article object in the database.
  */
 
  public function update() {
 
    // Does the Menu Item object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Menu Item::update(): Attempt to update an Menu Item object that does not have its ID property set.", E_USER_ERROR );
    
    // Update the Item Menu
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE menu_items SET name=:name,description=:description,price=:price,categoryID=:categoryID,menuID=:menuID,imageExtension=:imageExtension WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":name", $this->name, PDO::PARAM_STR );
    $st->bindValue( ":description", $this->description, PDO::PARAM_STR );
    $st->bindValue( ":price", $this->price, PDO::PARAM_STR );
    $st->bindValue( ":categoryID", $this->categoryID, PDO::PARAM_INT );
    $st->bindValue( ":menuID", $this->menuID, PDO::PARAM_INT );
    $st->bindValue( ":imageExtension", $this->imageExtension, PDO::PARAM_STR );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
 
  /**
  * Deletes the current Article object from the database.
  */
 
  public function delete() {
 
    // Does the Article object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "MenuItem::delete(): Attempt to delete a Menu Item object that does not have its ID property set.", E_USER_ERROR );
 
    // Delete the MenuItem
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM menu_items WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


   /**
  * Stores any image uploaded from the edit form
  *
  * @param assoc The 'image' element from the $_FILES array containing the file upload data
  */
 
  public function storeUploadedImage( $image ) {
 
    if ( $image['error'] == UPLOAD_ERR_OK )
    {
      // Does the Menu Item object have an ID?
      if ( is_null( $this->id ) ) trigger_error( "MenuItem::storeUploadedImage(): Attempt to upload an image for an Menu Item object that does not have its ID property set.", E_USER_ERROR );
 
      // Delete any previous image(s) for this article
      $this->deleteImages();
 
      // Get and store the image filename extension
      $this->imageExtension = strtolower( strrchr( $image['name'], '.' ) );
 
      // Store the image
 
      $tempFilename = trim( $image['tmp_name'] ); 
 
      if ( is_uploaded_file ( $tempFilename ) ) {
        if ( !( move_uploaded_file( $tempFilename, $this->getImagePath() ) ) ) trigger_error( "MenuItem::storeUploadedImage(): Couldn't move uploaded file.", E_USER_ERROR );
        if ( !( chmod( $this->getImagePath(), 0666 ) ) ) trigger_error( "MenuItem::storeUploadedImage(): Couldn't set permissions on uploaded file.", E_USER_ERROR );
      }
 
      // Get the image size and type
      $attrs = getimagesize ( $this->getImagePath() );
      $imageWidth = $attrs[0];
      $imageHeight = $attrs[1];
      $imageType = $attrs[2];
 
      // Load the image into memory
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          $imageResource = imagecreatefromgif ( $this->getImagePath() );
          break;
        case IMAGETYPE_JPEG:
          $imageResource = imagecreatefromjpeg ( $this->getImagePath() );
          break;
        case IMAGETYPE_PNG:
          $imageResource = imagecreatefrompng ( $this->getImagePath() );
          break;
        default:
          trigger_error ( "MenuItem::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      // Copy and resize the image to create the thumbnail
      $thumbHeight = intval ( $imageHeight / $imageWidth * MENUITEM_THUMB_WIDTH );
      $thumbResource = imagecreatetruecolor ( MENUITEM_THUMB_WIDTH, $thumbHeight );
      imagecopyresampled( $thumbResource, $imageResource, 0, 0, 0, 0, ARTICLE_THUMB_WIDTH, $thumbHeight, $imageWidth, $imageHeight );
 
      // Save the thumbnail
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          imagegif ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        case IMAGETYPE_JPEG:
          imagejpeg ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ), JPEG_QUALITY );
          break;
        case IMAGETYPE_PNG:
          imagepng ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        default:
          trigger_error ( "MenuItem::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      $this->update();
    }
  }
 
 
  /**
  * Deletes any images and/or thumbnails associated with the article
  */
 
  public function deleteImages() {
 
    // Delete all fullsize images for this items
    foreach (glob( MENUITEM_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "MenuItem::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
    }
     
    // Delete all thumbnail images for this article
    foreach (glob( MENUITEM_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "MenuItem::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
    }
 
    // Remove the image filename extension from the object
    $this->imageExtension = "";
  }
 
 
  /**
  * Returns the relative path to the article's full-size or thumbnail image
  *
  * @param string The type of image path to retrieve (IMG_TYPE_FULLSIZE or IMG_TYPE_THUMB). Defaults to IMG_TYPE_FULLSIZE.
  * @return string|false The image's path, or false if an image hasn't been uploaded
  */
 
  public function getImagePath( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension ) ? ( MENUITEM_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension ) : false;
  }
 
 
 
}
 
?>