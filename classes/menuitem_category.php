<?php
 
/**
 * Class to handle article categories
 */
 
class ItemCategory
{
  // Properties
 
  /**
  * @var int The item category ID from the database
  */
  public $id = null;
 
  /**
  * @var string Name of the item category
  */
  public $name = null;
 
  /**
  * @var string A short description of the item category
  */
  public $description = null;


   /**
  * @var int The resto ID from the database
  */
  public $restoId = null;
 
 
  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['name'] ) ) $this->name = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['name'] );
    if ( isset( $data['description'] ) ) $this->description = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['description'] );
    if ( isset( $data['restoId'] ) ) $this->restoId = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['restoId'] );
  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) {
 
    // Store all the parameters
    $this->__construct( $params );
  }
 
 
  /**
  * Returns a item Category object matching the given item category ID
  *
  * @param int The item category ID
  * @return Category|false The item category object, or false if the record was not found or there was a problem
  */
 
  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM item_categories WHERE id = :id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) return new ItemCategory( $row );
  }
 
 
  /**
  * Returns all (or a range of) item Category objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param string Optional column by which to order the categories (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Category objects; totalRows => Total number of categories
  */
 
  public static function getList( $restoId,$numRows=0, $perPage=10000000,$order="name ASC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM item_categories WHERE restoId = :restoId
            ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
    $st = $conn->prepare( $sql );
     $st->bindValue( ":restoId", $restoId, PDO::PARAM_INT );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
     $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $item_category = new ItemCategory( $row );
      $list[] = $item_category;
    }
 
    // Now get the total number of item categories that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }
 
 
  /**
  * Inserts the current item Category object into the database, and sets its ID property.
  */
 
  public function insert($id) {
 
    // Does the Category object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "ItemCategory::insert(): Attempt to insert a item Category object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Item Category
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO item_categories ( name, description, restoId) VALUES ( :name, :description, :restoId)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":name", $this->name, PDO::PARAM_STR );
    $st->bindValue( ":description", $this->description, PDO::PARAM_STR );
    $st->bindValue( ":restoId",$id, PDO::PARAM_INT );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }
 
 
  /**
  * Updates the current item Category object in the database.
  */
 
  public function update() {
 
    // Does the Category object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "ItemCategory::update(): Attempt to update a Category object that does not have its ID property set.", E_USER_ERROR );
    
    // Update the Category
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE item_categories SET name=:name, description=:description WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":name", $this->name, PDO::PARAM_STR );
    $st->bindValue( ":description", $this->description, PDO::PARAM_STR );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
 
  /**
  * Deletes the current item Category object from the database.
  */
 
  public function delete() {
 
    // Does the Category object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "ItemCategory::delete(): Attempt to delete a Category object that does not have its ID property set.", E_USER_ERROR );
 
    // Delete the Category
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM item_categories WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
}
 
?>