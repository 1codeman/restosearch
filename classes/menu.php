<?php
 
/**
 * Class to handle menu
 */
 
class Menu
{
  // Properties
 
  /**
  * @var int The item menu ID from the database
  */
  public $id = null;
 
  /**
  * @var string Name of the menu
  */
  public $menu_name = null;
 
  /**
  * @var string A short description of the menu
  */
  public $menu_description = null;

    /**
  * @var int The menu resto id
  */
  public $restoId = null;

 
 
  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['menu_name'] ) ) $this->menu_name = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['menu_name'] );
    if ( isset( $data['menu_description'] ) ) $this->menu_description = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['menu_description'] );
    if ( isset( $data['restoId'] ) ) $this->restoId = (int) $data['restoId'];
  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) {
 
    // Store all the parameters
    $this->__construct( $params );
  }
 
 
  /**
  * Returns a menu object matching the given menu ID
  *
  * @param int The menu ID
  * @return Menu|false The menu object, or false if the record was not found or there was a problem
  */
 
  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT * FROM menus WHERE restoId = :id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) return new Menu( $row );
  }
 
 
  /**
  * Returns all (or a range of) menu objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param string Optional column by which to order the categories (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Menu objects; totalRows => Total number of menu
  */
 
  public static function getList( $numRows=0, $perPage=10000000,$order="name ASC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM menus
            ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $menu = new Menu( $row );
      $list[] = $menu;
    }
 
    // Now get the total number of item categories that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }
 
 
  /**
  * Inserts the current item Category object into the database, and sets its ID property.
  */
 
  public function insert($id) {
 
    // Does the Menu object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "Menu::insert(): Attempt to insert a menu object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Item Category
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO menus ( menu_name, menu_description, restoId ) VALUES ( :name, :description, :restoId)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":name", $this->menu_name, PDO::PARAM_STR );
    $st->bindValue( ":description", $this->menu_description, PDO::PARAM_STR );
    $st->bindValue( ":restoId",$id, PDO::PARAM_INT );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }
 
 
  /**
  * Updates the current item Category object in the database.
  */
 
  public function update() {
 
    // Does the Menu object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Menu::update(): Attempt to update a Menu object that does not have its ID property set.", E_USER_ERROR );
    
    // Update the Menu
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE menus SET name=:name, description=:description, restoId = :restoId WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":name", $this->name, PDO::PARAM_STR );
    $st->bindValue( ":description", $this->description, PDO::PARAM_STR );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->bindValue( ":restoId",1, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
 
  /**
  * Deletes the current Menu object from the database.
  */
 
  public function delete() {
 
    // Does the Category object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "Menu::delete(): Attempt to delete a Category object that does not have its ID property set.", E_USER_ERROR );
 
    // Delete the Menu
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM menus WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
}
 
?>