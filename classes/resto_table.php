<?php
 
/**
 * Class to handle menu item
 */
 
class RestoTable
{
 
  // Properties
 
  /**
  * @var int The Resto Table ID 
  */
  public $id = null;

   /**
  * @var string name of the Resto Table
  */
  public $table_name = null;
 
  /**
  * @var string description of the Resto Table
  */
  public $table_description = null;
 

  /**
  * @var int capacity of resto table
  */
  public $capacity = null;


  /**
  * @var int resto Table resto ID
  */
  public $restoID = null;
 
  
  /**
  * @var int resto Table status
  */
  public $status = 0;
 
 
 
  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['table_name'] ) ) $this->table_name = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['table_name'] );
    if ( isset( $data['table_description'] ) ) $this->table_description = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['table_description'] );
    if ( isset( $data['capacity'] ) ) $this->capacity = (int) $data['capacity'];
    if ( isset( $data['restoID'] ) ) $this->restoID= (int) $data['restoID'];
    if ( isset( $data['status'] ) ) $this->status = (int) $data['status'];


  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) { //this function will handle the create and update menu item
 
    // Store all the parameters
    $this->__construct( $params );
 
  }
 
 
  /**
  * Returns an Menu Item object matching the given menu item ID
  *
  * @param int The Menu item ID
  * @return MenuItem|false The menu item object, or false if the record was not found or there was a problem
  */

  //static keyword to the method definitionallows the method to be called directly without specifying an object
 
  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD ); // makes a connection to the MySQL database using the login details from the config.php 
    $sql = "SELECT * FROM resto_tables WHERE id = :id"; 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null; //close connection
    if ( $row ) return new RestoTable( $row );
  }
 
 
   /**
  * Returns all (or a range of) Resto Table objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param int Optional Return just articles in the category with this ID
  * @param string Optional column by which to order the articles (default="table_name ASC")
  * @return Array|false A two-element array : results => array, a list of Article objects; totalRows => Total number of menu items
  */
 
  public static function getList( $numRows=0, $perPage=10000000, $restoID=null, $order="table_name ASC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM resto_tables WHERE restoID = :restoID
            ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->bindValue( ":restoID", $restoID, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $menuitem = new RestoTable( $row );
      $list[] = $menuitem;
    }
 
    // Now get the total number of articles that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }
 
 
  /**
  * Inserts the current Resto table object into the database, and sets its ID property.
  */
 
  public function insert() {
 
    // Does the Resto table object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "RestoTable::insert(): Attempt to insert an RestoTable object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Resto Table
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO resto_tables (table_name,table_description,capacity,restoID,status) VALUES (:table_name, :table_description, :capacity, :restoID, :status)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":table_name", $this->table_name, PDO::PARAM_STR );
    $st->bindValue( ":table_description", $this->table_description, PDO::PARAM_STR );
    $st->bindValue( ":capacity", $this->capacity, PDO::PARAM_INT);
    $st->bindValue( ":restoID",$this->restoID, PDO::PARAM_INT);
    $st->bindValue( ":status",0, PDO::PARAM_INT );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }
 
 
  /**
  * Updates the current Article object in the database.
  */
 
  public function update() {
 
    // Does the Resto Table object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "RestoTable::update(): Attempt to update an Resto Table object that does not have its ID property set.", E_USER_ERROR );
    
    // Update the Item Menu
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE resto_tables SET table_name=:table_name,table_description=:table_description,capacity=:capacity,restoID=:restoID,status=:status WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":table_name", $this->table_name, PDO::PARAM_STR );
    $st->bindValue( ":table_description", $this->table_description, PDO::PARAM_STR );
    $st->bindValue( ":capacity", $this->capacity, PDO::PARAM_INT );
    $st->bindValue( ":restoID", $this->restoID, PDO::PARAM_INT );
    $st->bindValue( ":status", $this->status, PDO::PARAM_INT );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
 
  /**
  * Deletes the current RestoTable object from the database.
  */
 
  public function delete() {
 
    // Does the Resto Table object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "RestoTable::delete(): Attempt to delete a Menu Item object that does not have its ID property set.", E_USER_ERROR );
 
    // Delete the RestoTable
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM resto_tables WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


   /**
  * Stores any image uploaded from the edit form
  *
  * @param assoc The 'image' element from the $_FILES array containing the file upload data
  */
 
  public function storeUploadedImage( $image ) {
 
    if ( $image['error'] == UPLOAD_ERR_OK )
    {
      // Does the Menu Item object have an ID?
      if ( is_null( $this->id ) ) trigger_error( "MenuItem::storeUploadedImage(): Attempt to upload an image for an Menu Item object that does not have its ID property set.", E_USER_ERROR );
 
      // Delete any previous image(s) for this article
      $this->deleteImages();
 
      // Get and store the image filename extension
      $this->imageExtension = strtolower( strrchr( $image['name'], '.' ) );
 
      // Store the image
 
      $tempFilename = trim( $image['tmp_name'] ); 
 
      if ( is_uploaded_file ( $tempFilename ) ) {
        if ( !( move_uploaded_file( $tempFilename, $this->getImagePath() ) ) ) trigger_error( "MenuItem::storeUploadedImage(): Couldn't move uploaded file.", E_USER_ERROR );
        if ( !( chmod( $this->getImagePath(), 0666 ) ) ) trigger_error( "MenuItem::storeUploadedImage(): Couldn't set permissions on uploaded file.", E_USER_ERROR );
      }
 
      // Get the image size and type
      $attrs = getimagesize ( $this->getImagePath() );
      $imageWidth = $attrs[0];
      $imageHeight = $attrs[1];
      $imageType = $attrs[2];
 
      // Load the image into memory
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          $imageResource = imagecreatefromgif ( $this->getImagePath() );
          break;
        case IMAGETYPE_JPEG:
          $imageResource = imagecreatefromjpeg ( $this->getImagePath() );
          break;
        case IMAGETYPE_PNG:
          $imageResource = imagecreatefrompng ( $this->getImagePath() );
          break;
        default:
          trigger_error ( "MenuItem::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      // Copy and resize the image to create the thumbnail
      $thumbHeight = intval ( $imageHeight / $imageWidth * MENUITEM_THUMB_WIDTH );
      $thumbResource = imagecreatetruecolor ( MENUITEM_THUMB_WIDTH, $thumbHeight );
      imagecopyresampled( $thumbResource, $imageResource, 0, 0, 0, 0, ARTICLE_THUMB_WIDTH, $thumbHeight, $imageWidth, $imageHeight );
 
      // Save the thumbnail
      switch ( $imageType ) {
        case IMAGETYPE_GIF:
          imagegif ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        case IMAGETYPE_JPEG:
          imagejpeg ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ), JPEG_QUALITY );
          break;
        case IMAGETYPE_PNG:
          imagepng ( $thumbResource, $this->getImagePath( IMG_TYPE_THUMB ) );
          break;
        default:
          trigger_error ( "MenuItem::storeUploadedImage(): Unhandled or unknown image type ($imageType)", E_USER_ERROR );
      }
 
      $this->update();
    }
  }
 
 
  /**
  * Deletes any images and/or thumbnails associated with the article
  */
 
  public function deleteImages() {
 
    // Delete all fullsize images for this items
    foreach (glob( MENUITEM_IMAGE_PATH . "/" . IMG_TYPE_FULLSIZE . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "MenuItem::deleteImages(): Couldn't delete image file.", E_USER_ERROR );
    }
     
    // Delete all thumbnail images for this article
    foreach (glob( MENUITEM_IMAGE_PATH . "/" . IMG_TYPE_THUMB . "/" . $this->id . ".*") as $filename) {
      if ( !unlink( $filename ) ) trigger_error( "MenuItem::deleteImages(): Couldn't delete thumbnail file.", E_USER_ERROR );
    }
 
    // Remove the image filename extension from the object
    $this->imageExtension = "";
  }
 
 
  /**
  * Returns the relative path to the article's full-size or thumbnail image
  *
  * @param string The type of image path to retrieve (IMG_TYPE_FULLSIZE or IMG_TYPE_THUMB). Defaults to IMG_TYPE_FULLSIZE.
  * @return string|false The image's path, or false if an image hasn't been uploaded
  */
 
  public function getImagePath( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension ) ? ( MENUITEM_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension ) : false;
  }
 
 
 
}
 
?>