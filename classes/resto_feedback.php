<?php
 
/**
 * Class to handle menu item
 */
 
class RestoFeedback
{
 
  // Properties
 
  /**
  * @var int The resto feedback ID 
  */
  public $id = null;

   /**
  * @var string feedback of the user in the resto
  */
  public $feedback = null;
 
 
  /**
  * @var float rating of the resto
  */
  public $rating  = null;


  /**
  * @var int feedback user ID
  */
  public $userID = null;


  /**
  * @var int feedback resto ID
  */
  public $restoID = null;
 

  /**
  * @var int When the feedback was published
  */
  public $publicationDate = null;

 

  public $uname = null;


  public $imageExtension = null;
  /**
  * Sets the object's properties using the values in the supplied array
  *
  * @param assoc The property values
  */
 
  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['feedback'] ) ) $this->feedback = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['feedback'] );
    if ( isset( $data['rating'] ) ) $this->rating = (float) $data['rating'];
    if ( isset( $data['userID'] ) ) $this->userID = (int) $data['categoryID'];
    if ( isset( $data['restoID'] ) ) $this->menuID = (int) $data['restoID'];
    if ( isset( $data['publicationDate'] ) ) $this->publicationDate = (int) $data['publicationDate'];
    if ( isset( $data['uname'] ) ) $this->uname = $data['uname'];
     if ( isset( $data['imageExtension'] ) ) $this->imageExtension = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\$ a-zA-Z0-9()]/", "", $data['imageExtension'] );
 
 

  }
 
 
  /**
  * Sets the object's properties using the edit form post values in the supplied array
  *
  * @param assoc The form post values
  */
 
  public function storeFormValues ( $params ) { //this function will handle the create and update menu item
 
    // Store all the parameters
    $this->__construct( $params );
    
 
    // /**
    // * Parse and store the publication date. It also Handles publication dates in the format YYYY-MM-DD, 
    // * converting the date into the UNIX timestamp format suitable for storing in the object.
    // */

    // if ( isset($params['publicationDate']) ) {
    //   $publicationDate = explode ( '-', $params['publicationDate'] );
 
    //   if ( count($publicationDate) == 3 ) {
    //     list ( $y, $m, $d ) = $publicationDate;
    //     $this->publicationDate = mktime ( 0, 0, 0, $m, $d, $y );
    //   }
    // }
  }
 
 
  /**
  * Returns an Menu Item object matching the given menu item ID
  *
  * @param int The Menu item ID
  * @return MenuItem|false The menu item object, or false if the record was not found or there was a problem
  */

  //static keyword to the method definitionallows the method to be called directly without specifying an object
 
  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD ); // makes a connection to the MySQL database using the login details from the config.php 
    $sql = "SELECT * FROM resto_feedbacks WHERE id = :id"; 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null; //close connection
    if ( $row ) return new RestoFeedback( $row );
  }
 
 
   /**
  * Returns all (or a range of) Menu Item objects in the DB
  *
  * @param int Optional The number of rows to return (default=all)
  * @param int Optional Return just articles in the category with this ID
  * @param string Optional column by which to order the articles (default="name ASC")
  * @return Array|false A two-element array : results => array, a list of Article objects; totalRows => Total number of menu items
  */
 
  public static function getList($userId,$restoId,$numRows=0, $perPage=10000000, $menuID=null, $order="publicationDate DESC" ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM resto_feedbacks LEFT JOIN  users ON resto_feedbacks.userID = users.id " .
    " WHERE resto_feedbacks.restoID =" . $restoId . " ORDER BY " . mysql_escape_string($order) . " LIMIT :numRows, :perPage";
 
 
 
    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->bindValue( ":perPage", $perPage, PDO::PARAM_INT );
    $st->execute();
    $list = array();
 
    while ( $row = $st->fetch() ) {
      $restofeedback = new RestoFeedback( $row );
      $list[] = $restofeedback;
    }
 
     // Now get the total number of articles that matched the criteria
    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
   
    
    //get rating sum
     $sql = "SELECT SUM(rating) as totalRatings FROM resto_feedbacks LEFT JOIN  users ON resto_feedbacks.userID = users.id" . 
    " WHERE resto_feedbacks.restoID =" . $restoId ;
    
    $totalRatings = $conn->query( $sql )->fetch();
    
    if($totalRows[0]!=0)
      $averageRatings =  $totalRatings[0] /  $totalRows[0];
  
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0] , "totalRatings" => $totalRatings[0] , "averageRatings" => $averageRatings) );
  }
 
 
  /**
  * Inserts the current Article object into the database, and sets its ID property.
  */
 
  public function insert($userID,$restoID) {
 
    // Does the Menu ITem object already have an ID?
    if ( !is_null( $this->id ) ) trigger_error ( "RestoFeedback::insert(): Attempt to insert an MenuItem object that already has its ID property set (to $this->id).", E_USER_ERROR );
 
    // Insert the Menu Item
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO resto_feedbacks (feedback,rating,userID,restoID,publicationDate) VALUES (:feedback, :rating, :userID, :restoID,:publicationDate)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":feedback", $this->feedback, PDO::PARAM_STR );
    $st->bindValue( ":rating", $this->rating, PDO::PARAM_STR );
    $st->bindValue( ":userID",$userID, PDO::PARAM_INT );
    $st->bindValue( ":restoID",$restoID, PDO::PARAM_INT );
    $st->bindValue( ":publicationDate", date('Y-m-d H:i:s'), PDO::PARAM_INT );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }
 
 
  /**
  * Updates the current Article object in the database.
  */
 
  public function update() {
 
    // Does the Menu Item object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "RestoFeedback::update(): Attempt to update an Menu Item object that does not have its ID property set.", E_USER_ERROR );
    
    // Update the Item Menu
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE resto_feedbacks SET feedback=:feedback,rating=:rating,userID=:userID,restoID=:restoID,publicationDate=:publicationDate WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":feedback", $this->feedback, PDO::PARAM_STR );
    $st->bindValue( ":rating", $this->rating, PDO::PARAM_STR );
    $st->bindValue( ":userID",$_SESSION['userId'], PDO::PARAM_INT );
    $st->bindValue( ":restoID", $_SESSION['restoId'], PDO::PARAM_INT );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->bindValue( ":publicationDate", date('Y-m-d H:i:s'), PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }
 
 
  /**
  * Deletes the current Article object from the database.
  */
 
  public function delete() {
 
    // Does the Article object have an ID?
    if ( is_null( $this->id ) ) trigger_error ( "RestoFeedback::delete(): Attempt to delete a Menu Item object that does not have its ID property set.", E_USER_ERROR );
 
    // Delete the MenuItem
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $st = $conn->prepare ( "DELETE FROM resto_feedbacks WHERE id = :id LIMIT 1" );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


   public function getImagePath( $type=IMG_TYPE_FULLSIZE ) {
    return ( $this->id && $this->imageExtension ) ? ( USER_IMAGE_PATH . "/$type/" . $this->id . $this->imageExtension ) : false;
  }
 
}
 
?>