<?php
 
require( "config.php" ); // include the config file so that all the configuration settings are available to the script
require_once "recaptchalib.php";
/**
* check that the $_GET['action'] value exists by using isset(). 
*If it doesn't, we set the corresponding $action variable to an empty string ("").
*/
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['user-username'] ) ? $_SESSION['user-username'] : "";

if (isset( $_SESSION['user-username'] ) && $action == 'user-login') {
     landingpage();
  exit;
}

// if ($action != "user-login" && $action != "user-logout" && $action != "user-signup" && !$username ) {
//     login();
//   exit;
// }
 // Decide which action to perform base on the $action value
switch ( $action ) {
   case 'user-login':
    login();
    break;
  case 'user-logout':
    logout();
    break;
  case 'user-signup':
    signup();
    break; 
  case 'profile':
    profilepage();
    break; 
  case 'archive':
    archive();
    break;
  case 'viewArticle':
    viewArticle();
    break;
  case 'viewResto':
    viewResto();
    break;
  case 'castFeedback':
    castFeedback();
    break;
  default:
    landingpage();
}
 
  
function  castFeedback() {
 
  $results = array();
//   $results['pageTitle'] = "New Article Category";
//   $results['formAction'] = "newCategory";
 
  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the category edit form: save the new category
    $feedback = new RestoFeedback;
    $feedback->storeFormValues( $_POST );
    $feedback->insert($_SESSION['userId'], $_SESSION['viewedRestoId']);
    $results['statusMessage'] ="Successfully Cast Your Feedback";
    header( "Location: .?action=viewResto&restoId=" . $_SESSION['viewedRestoId'] );
 
  } else {
 
    // // User has not posted the category edit form yet: display the form
    // $results['resto_feedback'] = new RestoFeedback;
    // require( TEMPLATE_PATH . "/landingpage.php" );
  }
 
}
 
 
 function login() {
 
  $results = array();
  $results['pageTitle'] = "User Login";
  $username = isset( $_POST['username'] ) ? $_POST['username']: "";
  $password = isset( $_POST['password'] ) ? $_POST['password']: "";


  
      if ( isset( $_POST['user-login'] ) ) {
          
          $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
           $sql = "SELECT * FROM users WHERE uname = :uname or email = :email LIMIT 1";
           $st = $conn->prepare( $sql );
           $st->bindValue( ":uname", $username, PDO::PARAM_STR);
           $st->bindValue( ":email", $username, PDO::PARAM_STR);
           $st->execute();
           $user=$st->fetch(PDO::FETCH_ASSOC);
        // User has posted the login form: attempt to log the user in
     
        if ($st->rowCount()>0) {
            
            if($user['password']==sha1($password))
            {
                      // Login successful: Create a session and redirect to the homepage
                  $_SESSION['user-username'] = $user['uname'];
                  $_SESSION['userId'] = $user['id'];
                  $_SESSION['user-email'] = $user['email'];
                  header( "Location: index.php" );
            }
            else
            {
                  // Login failed: display an error message to the user
              $results['errorMessage'] = "Incorrect username or password. Please try again.";
              require( TEMPLATE_PATH . "/user/loginForm.php" );
            }
         
     
        } else {
     
          // Login failed: display an error message to the user
          $results['errorMessage'] = "Incorrect username or password. Please try again.";
          require( TEMPLATE_PATH . "/user/loginForm.php" );
        }
     
      } else {
     
        // User has not posted the login form yet: display the form
        require( TEMPLATE_PATH . "/user/loginForm.php" );
      }
 
}

 function signup() {
  $uname = isset( $_POST['uname'] ) ? $_POST['uname']: "";
  $email = isset( $_POST['email'] ) ? $_POST['email']: "";
  $results = array();
  $results['pageTitle'] = "User Signup";


  // // your secret key
  // $secret = "6LfEFQwTAAAAAGNUT7yin4y2udoEQQniyWIZH9K8";
  // // empty response
  $response = "asdads";
  // // check secret key
  // $reCaptcha = new ReCaptcha($secret);

  //   // if submitted check response
  // if ($_POST["g-recaptcha-response"]) {
  //     $response = $reCaptcha->verifyResponse(
  //         $_SERVER["REMOTE_ADDR"],
  //         $_POST["g-recaptcha-response"]
  //     );
  // }else{
  //    $results['errorMessage'] = "Robot ka ba?!";
  // }

// if ($response != null && $response->success) { 
  try
      {
         $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
         $sql = "SELECT * FROM users WHERE uname = :uname or email = :email";
         $st = $conn->prepare( $sql );
         $st->bindValue( ":uname", $uname, PDO::PARAM_STR);
         $st->bindValue( ":email", $email, PDO::PARAM_STR);
         $st->execute();
         $row=$st->fetch(PDO::FETCH_ASSOC);
    
         if($row['uname']==$uname) {
            $results['errorMessage'] = "sorry username already taken !";
         }
         else if($row['email']==$email) {
             $results['errorMessage']  = "sorry email id already taken !";
         }
         else
        {
           $user = new User;
           $user->storeFormValues( $_POST );
           $id = $user->insert();
           $results['statusMessage'] = "You have already Sign Up.";
           $results['userid'] = $id;
              
        }

      
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
 // }
   require( TEMPLATE_PATH . "/user/loginForm.php" );
    
}
 
 
function logout() {
  unset( $_SESSION['user-username'] );
  header( "Location: index.php" );
}



 function archive() {
     
  $results = array();
  $results['user'] = User::getByUsername( $_SESSION['user-username']);
  $categoryId = ( isset( $_GET['categoryId'] ) && $_GET['categoryId'] ) ? (int)$_GET['categoryId'] : null;
  $results['category'] = Category::getById( $categoryId );
  $data = Article::getList(0, 100000, $results['category'] ? $results['category']->id : null );
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
  $results['pageHeading'] = $results['category'] ?  $results['category']->name : "All ";
  $results['pageTitle'] = $results['pageHeading'] . "Articles";
  require( TEMPLATE_PATH . "/archive.php" );
}


// This function displays a list of all the resto in the database. 
// calling the getList() method of the Resto class
function viewResto() {
  if ( !isset($_GET["restoId"]) || !$_GET["restoId"] ) {
   landingpage();
   return;
  }
 
  $results = array();
  $results['user'] = User::getByUsername( $_SESSION['user-username']);

  $results['resto'] = Resto::getById( (int)$_GET["restoId"] );
  
  //$results['category'] = Category::getById( $results['article']->categoryId );
  
  $results['menu'] = Menu::getById( (int)$_GET["restoId"] );

//   $record_per_page = 5;  
//   if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
//   $start_from = ($page-1) * $record_per_page;  

  //get menu items
  $data = MenuItem::getList(0,9999,$results['menu']->id);
  
  $results['resto'] = Resto::getById((int)$_GET["restoId"]);
  $results['menuitems'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  
    //get 3 menu items
  $data = MenuItem::getList(0,3,$results['menu']->id);
  $results['3menuitems'] = $data['results'];
 
  

  
  $results['pageTitle'] = $results['resto']->resto_name;
  $_SESSION['viewedRestoId'] = $_GET["restoId"];
  
   //get feedbacks
  $data2 = RestoFeedback::getList($_SESSION['userId'],$_SESSION['viewedRestoId'],0,1000000);
  $results['restofeedbacks'] = $data2['results'];
  $results['totalRatingRows'] = $data2['totalRows'];
   $results['totalRatings'] = $data2['totalRatings'];
  
  
       //get 3 Articles
  $data = Article::getList(0,3,null,$_SESSION['owner-id']);
  $results['articles'] = $data['results'];
  require( TEMPLATE_PATH . "/viewResto.php" );
}
 

// This function displays a list of all the articles in the database. 
// calling the getList() method of the Article class
function viewArticle() {
  if ( !isset($_GET["articleId"]) || !$_GET["articleId"] ) {
    homepage();
    return;
  }
 
  $results = array();
  $results['article'] = Article::getById( (int)$_GET["articleId"] );
  $results['category'] = Category::getById( $results['article']->categoryId );
  $results['pageTitle'] = $results['article']->title . " | #NCFF";
  require( TEMPLATE_PATH . "/viewArticle.php" );
}
 
function homepage() {
  $results = array();
  $results['user'] = User::getByUsername( $_SESSION['user-username']);
  $data = Article::getList(0, HOMEPAGE_NUM_ARTICLES );
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $data = Category::getList();
  $results['categories'] = array();
  foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category; 
  $results['pageTitle'] = "#NCFF";
  require( TEMPLATE_PATH . "/homepage.php" );
}

 
function landingpage() {
  $results = array();
  $results['user'] = User::getByUsername( $_SESSION['user-username']);
  $data = Resto::getList(0,1000000 );
  $results['restos'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  // $data = Category::getList();
  // $results['categories'] = array();
  // foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category; 
  
 
  
  $results['pageTitle'] = "#NCFF";
  require( TEMPLATE_PATH . "/landingpage.php" );
}


function profilepage() {
    

  $results = array();
    $results['pageTitle'] = "My Profile";
  $results['formAction'] = "profile";
//   $results['resto'] = Resto::getById($_SESSION['restoId']);

    if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "ownerNotFound" ) $results['errorMessage'] = "Error: Owner not found.";
    if ( $_GET['error'] == "ownerContainsItems" ) $results['errorMessage'] = "Error: Item Category contains articles. Delete the articles, or assign them to another category, before deleting this category.";
  } 
 
  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your Profile has been updated.";
  }

  if ( isset( $_POST['saveChanges'] ) ) {
 
    // User has posted the owner edit form: save the owner changes
 
    if ( !$user = User::getByUsername( $_POST['uname'] ) ) {
      header( "Location: index.php?action=profile&error=OwnerNotFound" );
      return;
    }
 
    $user->storeFormValues( $_POST );  
    if ( isset($_POST['deleteImage']) && $_POST['deleteImage'] == "yes" ) $user->deleteImages();
    $user->update();
    if ( isset( $_FILES['image'] ) ) $user->storeUploadedImage( $_FILES['image'] );
    header( "Location: index.php?action=profile&status=changesSaved" );
 
  } elseif ( isset( $_POST['cancel'] ) ) {
 
    // Owner has cancelled their edits: return to the category list
    header( "Location: index.php?action=profile" );
  } else {
 
    // User has not posted the owner edit form yet: display the form
    $results['user'] = User::getByUsername( $_SESSION['user-username']);
    require( TEMPLATE_PATH . "/user/profile.php" );
  }

}
 
?>